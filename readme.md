Hampel Linode DNS Manager
=========================

A web application to help you manage large collections of domain DNS entries hosted on Linode.

By [Simon Hampel](http://hampelgroup.com/).

About
-----

The [Linode DNS Manager](https://manager.linode.com/dns/index/) provided by Linode for free to its members
(see documentation here: [Linode DNS Manager](https://library.linode.com/dns-manager)) is very easy to use and more than capable of managing
your DNS records for you.

However, sometimes there are tasks you frequently need to perform which could do with some degree of automation to save time and help avoid mistakes when editing DNS
entries. This is especially true for very large collections of domains.

With my own domain portfolio, I was finding myself spending a lot of time performing the same DNS management operations over and over again.
With a large collection of domains, even simple tasks such as changing the IP address of the web server was an annoyingly time-consuming chore.

It also became cumbersome to manage so many domains, especially when many of them are simply "aliases" of one of my main domains (ie. same base-name, different TLD,
for example, hampelgroup.com.au and hampelgroup.net.au are both aliases of my main hampelgroup.com domain). The index screen showing my domains in Linode DNS manager is
very, very long!

So when I discovered the [Linode API](https://www.linode.com/api/), I decided to try creating a simple web application to help me manage my domains more efficiently.

My first try was a WordPress theme I hacked to display domain entries and provide a few tools to help manage them. WordPress made it easy to get started, but the code
was quite a mess and WordPress is not really designed to be a web application framework. Even so, I have been using this tool successfully for several years now.

However, the limitations of my original application were starting to frustrate me and I wasn't interested in further hacking a WordPress-based solution.
I also wanted to learn a new development framework ([Laravel](http://laravel.com/)) and a UI framework ([Zurb Foundation](http://foundation.zurb.com/)),
so I decided to re-build my DNS management tool from scratch and use this as a learning exercise.

I now use this new tool regularly and find it helps keep my domain portfolio under control. I'm always looking for ways to improve it too, so if you have any great ideas
about how to make it even better, please [let me know](mailto:simon@hampelgroup.com).

Installation
------------

...


Configuration
-------------

...

Usage
-----

...

Notes
-----

The application has these current limitations:

* Only one user login (although you can manually create additional users with the same full access rights)
* No support for SRV records
* No support for NS records
* No support for Master/Slave zones
* No support for AXFR entries
* No support for cloning zone entries
* Incomplete internationalisation of language files
