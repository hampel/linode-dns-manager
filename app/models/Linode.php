<?php
/**
 * 
 */

use Hampel\Linode\Service\LinodeException;

class Linode
{

	protected static function fetchLinodeDomainMap()
	{
		try
		{
			$linode_domains = App::make('linode.domain')->list();

			$domain_map = array();
			foreach ($linode_domains as $linodeid => $domain)
			{
				Cache::add('linode_' . $linodeid, $domain, Config::get('linode.cache_expiry'));

				$domain_map[$linodeid] = $domain->getDomainName();
			}
			return $domain_map;
		}
		catch (LinodeException $e)
		{
			Log::error("Could not retrieve domains list from Linode: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return null;
		}
	}

	public static function getLinodeDomainMap()
	{
		return Cache::remember('linode_map', Config::get('linode.cache_expiry'), function()
		{
			return self::fetchLinodeDomainMap();
		});
	}

	public static function refreshLinodeDomainMap()
	{
		Cache::forget('linode_map');
		return self::getLinodeDomainMap();
	}

	public static function getUnmanagedDomains($domains)
	{
		$unmanaged_domains = array();

		$managed_ids = array();
		foreach ($domains as $domain)
		{
			$managed_ids[] = $domain->linodeid;
		}

		$linode_map = self::getLinodeDomainMap();
		foreach ($linode_map as $linodeid => $name)
		{
			if (!in_array($linodeid, $managed_ids)) $unmanaged_domains[$name] = $linodeid;
		}

		return $unmanaged_domains;
	}

	public static function checkUnmanaged($base, $tlds)
	{
		$unmanaged = array();
		$reverse_map = array_flip(self::getLinodeDomainMap());
		foreach ($tlds as $tld)
		{
			if (array_key_exists($base . $tld, $reverse_map)) $unmanaged[] = $tld;
		}

		return $unmanaged;
	}

	public static function getOrphanedDomains($domains)
	{
		$orphaned_domains = array();

		$linode_map = self::getLinodeDomainMap();

		foreach ($domains as $domain)
		{
			if (!array_key_exists($domain->linodeid, $linode_map))
			{
				$orphaned_domains[$domain->name] = $domain->id;
			}
		}

		return $orphaned_domains;
	}

	public static function findUpdatedDomain($name, $id)
	{
		$linode_map = self::getLinodeDomainMap();
		foreach ($linode_map as $linodeid => $linodename)
		{
			if ($linodeid == $id) continue;
			if ($name == $linodename) return $linodeid;
		}

		return null;
	}

	public static function getDomainParents($domains)
	{
		$parents = array();

		foreach ($domains as $domain)
		{
			if (!array_key_exists($domain->linodeid, self::getLinodeDomainMap())) continue; // skip orphaned domains
			if ($domain->parent > 0) continue; // skip domains with parents, we wan't the parents themselves

			$linode = self::getLinodeDomain($domain->linodeid);
			if (empty($linode)) continue; // oops ... skip this one (shouldn't happen!)

			$domain->type = $linode->getDomainType();
			$parents[$domain->name] = $domain;
		}

		return $parents;
	}

	public static function getAliasCount($domains)
	{
		$parents = array();

		foreach ($domains as $domain)
		{
			if (!array_key_exists($domain->linodeid, self::getLinodeDomainMap())) continue; // skip orphaned domains

			if ($domain->parent > 0)
			{
				if (array_key_exists($domain->parent, $parents)) $parents[$domain->parent]++;
				else $parents[$domain->parent] = 1;
			}
			else
			{
				if (!array_key_exists($domain->id, $parents)) $parents[$domain->id] = 0;
			}
		}

		return $parents;
	}

	protected static function fetchLinodeDomain($linodeid)
	{
		try
		{
			return App::make('linode.domain')->list($linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not retrieve domain {$linodeid} from Linode: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return null;
		}
	}

	public static function getLinodeDomain($linodeid)
	{
		if (empty($linodeid)) return null;

		return Cache::remember('linode_' . $linodeid, Config::get('linode.cache_expiry'), function() use ($linodeid)
		{
			return self::fetchLinodeDomain($linodeid);
		});
	}

	public static function refreshLinodeDomain($linodeid)
	{
		Cache::forget('linode_' . $linodeid);
		self::getLinodeDomain($linodeid);
	}

	public static function createDomainFromLinode($linodeid = 0, $parent = 0, $name = "")
	{
		$domain = new Domain;

		if ($linodeid > 0)
		{
			$linode = self::getLinodeDomain($linodeid);
			if (empty($linode)) return null;

			$domain->name = $linode->getDomainName();
			$domain->linodeid = $linode->getDomainId();
		}
		elseif (!empty($name))
		{
			$domain_map = array_flip(self::getLinodeDomainMap());
			if (array_key_exists($name, $domain_map)) $domain->linodeid = $domain_map[$name];
			$domain->name = $name;
		}

		if ($parent > 0)
		{
			$domain->parent = $parent;
		}

		return $domain;
	}

	public static function createLinodeDomain($name)
	{
		$options = array(
			'soa_email' => Config::get('linode.soa_email'),
			'ttl_sec' => Config::get('linode.default_ttl'),
		);

		try
		{
			$linodeid = App::make('linode.domain')->create($name, 'master', $options);

			// update cache
			self::refreshLinodeDomainMap();

			return $linodeid;
		}
		catch (LinodeException $e)
		{
			Log::error("Could not create domain {$name}: " . $e->getMessage(), array('error_code' => $e->getCode()));
			throw $e;
		}
	}

	public static function storeDomain($input)
	{
		$domain = new Domain($input);

		if ($domain->linodeid == 0)
		{
			try
			{
				// not working with an existing linode domain entry, let's create a new one
				$domain->linodeid = self::createLinodeDomain($domain->name);
			}
			catch (LinodeException $e)
			{
				return Redirect::action('DomainController@create')
					->with('error', true)
					->with('reason', 'linode.create.failure')
					->with('error_data', array('message' => $e->getMessage()))
					->withInput($input);
			}
		}

		// save our domain data to the database
		$domain->save();
		return Redirect::action('DomainController@show', array($domain->id))
			->with('success', "Domain {$domain->name} added to database");
	}

	public static function recreateDomain($domain)
	{
		try
		{
			$linodeid = self::createLinodeDomain($domain->name);
			$domain->linodeid = $linodeid;
			$domain->save();
		}
		catch (LinodeException $e)
		{
			return Redirect::action('DomainController@edit', array('domain' => $domain->id))
				->with('error', true)
				->with('reason', 'linode.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('DomainController@show', array($domain->id))
			->with('success', "Domain {$domain->name} re-created on Linode");
	}

	public static function deleteDomain($domain)
	{
		$andlinode = "";

		if ($domain->linodeid > 0)
		{
			// check domain is in our map
			if (array_key_exists($domain->linodeid, self::getLinodeDomainMap()))
			{
				try
				{
					App::make('linode.domain')->delete($domain->linodeid);

					// update cache
					self::refreshLinodeDomainMap();
					Cache::forget('linode_' . $domain->linodeid);

					$andlinode = " and Linode";
				}
				catch (LinodeException $e)
				{
					Log::error("Could not delete domain {$domain->linodeid}: " . $e->getMessage(), array('error_code' => $e->getCode()));
					return Redirect::action('DomainController@show', array($domain->id))
						->with('error', true)
						->with('reason', 'linode.delete.failure')
						->with('error_data', array('message' => $e->getMessage()));
				}
			}
		}

		$domain->delete();
		return Redirect::action('DomainController@index')
			->with('success', "Domain {$domain->name} deleted from database{$andlinode}");
	}

	public static function deleteFromLinode($linode)
	{
		$linodeid = $linode->getDomainId();

		try
		{
			App::make('linode.domain')->delete($linodeid);

			// update cache
			self::refreshLinodeDomainMap();
			Cache::forget('linode_' . $linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not delete domain {$linode->getDomainName()} from Linode: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return Redirect::action('LinodeController@delete', array($linodeid))
				->with('error', true)
				->with('reason', 'linode.delete.failure')
				->with('error_data', array('message' => $e->getMessage()));
		}

		return Redirect::action('DomainController@index')
			->with('success', "Domain {$linode->getDomainName()} deleted from Linode");
	}
}

?>
