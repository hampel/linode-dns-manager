<?php
/**
 * 
 */

use Hampel\Linode\Service\LinodeException;

class Resource
{
	public static function getResource($linodeid, $resourceid)
	{
		if (empty($linodeid) OR empty($resourceid)) return null;

		$resources = self::getAllResources($linodeid);
		if (empty($resources)) return null;

		if (!array_key_exists($resourceid, $resources)) return null;
		return $resources[$resourceid];
	}

	public static function getResourcesOfType($type, $linodeid)
	{
		if (empty($linodeid)) return null;

		$resources = self::getAllResources($linodeid);
		if (empty($resources)) return null;

		$resource_array = array();
		foreach ($resources as $resource)
		{
			if ($resource->getType() == $type)
			{
				$resource_array[$resource->getResourceId()] = $resource;
			}
		}

		return $resource_array;
	}

	public static function getAllResources($linodeid)
	{
		if ($linodeid == 0) return null;

		$resources = Cache::remember('resources_' . $linodeid, Config::get('linode.cache_expiry'), function() use ($linodeid)
		{
			return self::fetchResources($linodeid);
		});

		return $resources;
	}

	public static function refreshResourceCache($linodeid)
	{
		if (empty($linodeid)) return null;

		if (!array_key_exists($linodeid, Linode::getLinodeDomainMap()))
		{
			// we have an orphaned domain ... make sure we remove any cache entries
			Cache::forget('linode_' . $linodeid);
		}

		Cache::forget('resources_' . $linodeid);

		return self::getAllResources($linodeid);
	}

	public static function fetchResources($linodeid)
	{
		if ($linodeid == 0) return null;

		try
		{
			$resources = App::make('linode.domain.resource')->list($linodeid);
			return $resources;
		}
		catch (LinodeException $e)
		{
			Log::error("Could not retrieve resource list for domain {$linodeid} from Linode: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return null;
		}
	}

	public static function getTimes()
	{
		return array(
			'0' => 'Default',
			'300' => '300 (5 minutes)',
			'3600' => '3600 (1 hour)',
			'7200' => '7200 (2 hours)',
			'14400' => '14400 (4 hours)',
			'28800' => '28800 (8 hours)',
			'57600' => '57600 (16 hours)',
			'86400' => '86400 (1 day)',
			'172800' => '172800 (2 days)',
			'345600' => '345600 (4 days)',
			'604800' => '604800 (1 week)',
			'1209600' => '1209600 (2 weeks)',
			'2419200' => '2419200 (4 weeks)'
		);
	}

	public static function getProtocols()
	{
		return array(
			'tcp' => 'tcp',
			'udp' => 'udp',
			'xmpp' => 'xmpp',
			'tls' => 'tls',
		);
	}
}

?>
