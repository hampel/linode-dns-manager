<?php
/**
 * 
 */

class Domain extends Eloquent
{
	protected $fillable = array('name', 'parent', 'linodeid');

	public function save(array $options = array())
	{
		$this->base = $this->getDomainBase();

		parent::save($options);
	}

	public function getDomainBase()
	{
		return substr($this->name, 0, strpos($this->name, '.'));
	}

	public function getDomainTLD()
	{
		return substr($this->name, strpos($this->name, '.'));
	}

	public static function getAvailableAliases($base)
	{
		$tlds = array();
		$domains = Domain::where('base', '=', $base)->orderBy('name')->get();
		foreach ($domains as $domain)
		{
			$tlds[] = $domain->getDomainTLD();
		}

		$available_tlds = array();
		foreach (Config::get('linode.bulk_tlds') as $tld)
		{
			if (!in_array($tld, $tlds)) $available_tlds[] = $tld;
		}

		return $available_tlds;
	}

	public static function createAliases($domain)
	{
		if (!Input::has('tld'))
		{
			return Redirect::action('DomainController@show', array($domain->id))
				->with('error', true)
				->with('reason', 'linode.alias.empty');
		}

		$tlds = Config::get('linode.bulk_tlds');

		$names = array();

		foreach (Input::get('tld') as $tld)
		{
			if (!in_array($tld, $tlds))
			{
				return Redirect::action('DomainController@show', array($domain->id))
					->with('error', true)
					->with('reason', 'linode.alias.invalid')
					->with('error_data', array('tld' => $tld));
			}

			$alias = Linode::createDomainFromLinode(0, $domain->parent > 0 ? $domain->parent : $domain->id, $domain->base . $tld);

			if (!isset($alias->linodeid) OR $alias->linodeid == 0)
			{
				try
				{
					$alias->linodeid = Linode::createLinodeDomain($alias->name);
				}
				catch (LinodeException $e)
				{
					return Redirect::action('DomainController@show', array($domain->id))
						->with('error', true)
						->with('reason', 'linode.create.failure')
						->with('error_data', array('message' => $e->getMessage()))
						->withInput($input);
				}
			}

			$alias->save();

			$names[] = $alias->name;
		}

		return Redirect::action('DomainController@show', array($domain->id))
			->with('success', "Domain aliases " . implode(", ", $names) . " successfully created");
	}

	public static function cleanInvalidDomains()
	{
		$rows = self::where('linodeid', '=', 0)->delete();
		if ($rows > 0) Log::info("Removed {$rows} invalid domain entries");
		return $rows;
	}

	public static function getAllParents($name)
	{
		$domains = self::where('parent', 0)->orderBy('name')->get();

		$parents = array(0 => '- no parent -');
		foreach ($domains as $domain)
		{
			if ($name == $domain->name) continue; // skip this domain, don't want to be our own parent
			$parents[$domain->id] = $domain->name;
		}

		return $parents;
	}

	public static function validateCreateDomain($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'name'  => array('required', 'unique:domains', 'domain'),
			'parent' => array('integer', 'exists_or_zero:domains,id'),
			'linodeid'  => array('integer')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function validateUpdateDomain($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'parent' => array('integer', 'exists_or_zero:domains,id'),
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}
}

?>
