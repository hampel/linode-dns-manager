<?php namespace Linode;
/**
 *
 */

use App;
use Redirect;
use Resource;
use Validator;
use Hampel\Linode\Service\LinodeException;

class Srv extends \Eloquent
{
	protected $fillable = array('domainid', 'resourceid', 'name', 'target', 'protocol', 'priority', 'weight', 'port', 'ttl_sec');

	public function save(array $options = array())
	{
		if ($this->resourceid > 0)
		{
			return App::make('linode.domain.resource')->updateSrv($this->domainid, $this->resourceid, $this->name, $this->target, $this->protocol, $this->priority, $this->weight, $this->port, $this->ttl_sec);
		}
		else
		{
			return App::make('linode.domain.resource')->createSrv($this->domainid, $this->name, $this->target, $this->protocol, $this->priority, $this->weight, $this->port);
		}
	}

	public function delete()
	{
		return App::make('linode.domain.resource')->delete($this->domainid, $this->resourceid);
	}

	public static function getRecord($linodeid, $resourceid)
	{
		return Resource::getResource($linodeid, $resourceid);
	}

	public static function getAllRecords($linodeid)
	{
		return Resource::getResourcesOfType('SRV', $linodeid);
	}

	public static function validateSrv($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'name' => array('required'),
			'target' => array('required'),
			'priority' => array('integer', 'between:0,255'),
			'weight' => array('integer', 'between:0,255'),
			'port' => array('integer', 'between:0,65535'),
			'ttl_sec' => array('integer', 'min:0')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function storeSrv($linodeid, $input)
	{
		$srv = new Srv($input);
		$srv->domainid = $linodeid;
		try
		{
			$srv->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\SrvController@create')
				->with('error', true)
				->with('reason', 'srv.create.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "SRV record for domain {$linodeid} created on Linode");
	}

	public static function updateSrv($linodeid, $resourceid, $input)
	{
		$srv = new Srv($input);
		$srv->domainid = $linodeid;
		$srv->resourceid = $resourceid;
		try
		{
			$srv->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\SrvController@edit')
				->with('error', true)
				->with('reason', 'srv.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "SRV record for domain {$linodeid} updated on Linode");
	}

	public static function deleteRecord($linodeid, $resourceid, $resource)
	{
		$srv = new Srv($resource->get());

		try
		{
			$srv->delete();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not delete SRV record {$resourceid}: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return Redirect::action('Linode\SrvController@delete', array($linodeid, $resourceid))
				->with('error', true)
				->with('reason', 'srv.delete.failure')
				->with('error_data', array('message' => $e->getMessage()));
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "SRV record successfully deleted");
	}
}

?>
