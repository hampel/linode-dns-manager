<?php namespace Linode;
/**
 *
 */

use App;
use Redirect;
use Resource;
use Validator;
use Hampel\Linode\Service\LinodeException;

class Aaaa extends \Eloquent
{
	protected $fillable = array('domainid', 'resourceid', 'name', 'target', 'ttl_sec');

	public function save(array $options = array())
	{
		if ($this->resourceid > 0)
		{
			return App::make('linode.domain.resource')->updateAaaa($this->domainid, $this->resourceid, $this->name, $this->target, $this->ttl_sec);
		}
		else
		{
			return App::make('linode.domain.resource')->createAaaa($this->domainid, $this->name, $this->target);
		}
	}

	public function delete()
	{
		return App::make('linode.domain.resource')->delete($this->domainid, $this->resourceid);
	}

	public static function getRecord($linodeid, $resourceid)
	{
		return Resource::getResource($linodeid, $resourceid);
	}

	public static function getAllRecords($linodeid)
	{
		return Resource::getResourcesOfType('AAAA', $linodeid);
	}

	public static function validateAaaa($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'target' => array('required', 'ipv6_public'),
			'ttl_sec' => array('integer', 'min:0')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function storeAaaa($linodeid, $input)
	{
		$aaaa = new Aaaa($input);
		$aaaa->domainid = $linodeid;
		try
		{
			$aaaa->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\AAAAController@create')
				->with('error', true)
				->with('reason', 'aaaa.create.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "AAAA record for domain {$linodeid} created on Linode");
	}

	public static function updateAaaa($linodeid, $resourceid, $input)
	{
		$aaaa = new Aaaa($input);
		$aaaa->domainid = $linodeid;
		$aaaa->resourceid = $resourceid;
		try
		{
			$aaaa->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\AAAAController@edit')
				->with('error', true)
				->with('reason', 'aaaa.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "AAAA record for domain {$linodeid} updated on Linode");
	}

	public static function deleteRecord($linodeid, $resourceid, $resource)
	{
		$aaaa = new Aaaa($resource->get());

		try
		{
			$aaaa->delete();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not delete AAAA record {$resourceid}: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return Redirect::action('Linode\AAAAController@delete', array($linodeid, $resourceid))
				->with('error', true)
				->with('reason', 'aaaa.delete.failure')
				->with('error_data', array('message' => $e->getMessage()));
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "AAAA record successfully deleted");
	}
}

?>
