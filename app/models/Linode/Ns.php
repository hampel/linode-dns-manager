<?php namespace Linode;
/**
 *
 */

use App;
use Redirect;
use Resource;
use Validator;
use Hampel\Linode\Service\LinodeException;

class Ns extends \Eloquent
{
	protected $fillable = array('domainid', 'resourceid', 'target', 'name', 'ttl_sec');

	public function save(array $options = array())
	{
		if ($this->resourceid > 0)
		{
			return App::make('linode.domain.resource')->updateNs($this->domainid, $this->resourceid, $this->target, $this->name, $this->ttl_sec);
		}
		else
		{
			return App::make('linode.domain.resource')->createNs($this->domainid, $this->target, $this->name);
		}
	}

	public function delete()
	{
		return App::make('linode.domain.resource')->delete($this->domainid, $this->resourceid);
	}

	public static function getRecord($linodeid, $resourceid)
	{
		return Resource::getResource($linodeid, $resourceid);
	}

	public static function getAllRecords($linodeid)
	{
		return Resource::getResourcesOfType('NS', $linodeid);
	}

	public static function validateNs($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'target' => array('required', 'domain'),
			'ttl_sec' => array('integer', 'min:0')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function storeNs($linodeid, $input)
	{
		$ns = new Ns($input);
		$ns->domainid = $linodeid;
		try
		{
			$ns->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\NsController@create')
				->with('error', true)
				->with('reason', 'ns.create.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "NS record for domain {$linodeid} created on Linode");
	}

	public static function updateNs($linodeid, $resourceid, $input)
	{
		$ns = new Ns($input);
		$ns->domainid = $linodeid;
		$ns->resourceid = $resourceid;
		try
		{
			$ns->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\NsController@edit')
				->with('error', true)
				->with('reason', 'ns.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "NS record for domain {$linodeid} updated on Linode");
	}

	public static function deleteRecord($linodeid, $resourceid, $resource)
	{
		$ns = new Ns($resource->get());

		try
		{
			$ns->delete();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not delete NS record {$resourceid}: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return Redirect::action('Linode\NsController@delete', array($linodeid, $resourceid))
				->with('error', true)
				->with('reason', 'ns.delete.failure')
				->with('error_data', array('message' => $e->getMessage()));
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "NS record successfully deleted");
	}
}

?>
