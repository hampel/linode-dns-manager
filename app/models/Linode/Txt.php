<?php namespace Linode;
/**
 *
 */

use App;
use Redirect;
use Resource;
use Validator;
use Hampel\Linode\Service\LinodeException;

class Txt extends \Eloquent
{
	protected $fillable = array('domainid', 'resourceid', 'name', 'target', 'ttl_sec');

	public function save(array $options = array())
	{
		if ($this->resourceid > 0)
		{
			return App::make('linode.domain.resource')->updateTxt($this->domainid, $this->resourceid, $this->name, $this->target, $this->ttl_sec);
		}
		else
		{
			return App::make('linode.domain.resource')->createTxt($this->domainid, $this->name, $this->target);
		}
	}

	public function delete()
	{
		return App::make('linode.domain.resource')->delete($this->domainid, $this->resourceid);
	}

	public static function getRecord($linodeid, $resourceid)
	{
		return Resource::getResource($linodeid, $resourceid);
	}

	public static function getAllRecords($linodeid)
	{
		return Resource::getResourcesOfType('TXT', $linodeid);
	}

	public static function validateTxt($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'ttl_sec' => array('integer', 'min:0')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function storeTxt($linodeid, $input)
	{
		$txt = new Txt($input);
		$txt->domainid = $linodeid;
		try
		{
			$txt->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\TxtController@create')
				->with('error', true)
				->with('reason', 'txt.create.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "TXT record for domain {$linodeid} created on Linode");
	}

	public static function updateTxt($linodeid, $resourceid, $input)
	{
		$txt = new Txt($input);
		$txt->domainid = $linodeid;
		$txt->resourceid = $resourceid;
		try
		{
			$txt->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\TxtController@edit')
				->with('error', true)
				->with('reason', 'txt.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "TXT record for domain {$linodeid} updated on Linode");
	}

	public static function deleteRecord($linodeid, $resourceid, $resource)
	{
		$txt = new Txt($resource->get());

		try
		{
			$txt->delete();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not delete TXT record {$resourceid}: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return Redirect::action('Linode\TxtController@delete', array($linodeid, $resourceid))
				->with('error', true)
				->with('reason', 'txt.delete.failure')
				->with('error_data', array('message' => $e->getMessage()));
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "TXT record successfully deleted");
	}
}

?>
