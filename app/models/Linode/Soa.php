<?php namespace Linode;
/**
 * 
 */

use App;
use Redirect;
use Resource;
use Validator;
use Hampel\Linode\Service\LinodeException;

class Soa extends \Eloquent
{
	protected $fillable = array('domainid', 'domain', 'description', 'type', 'soa_email', 'refresh_sec', 'retry_sec', 'expire_sec', 'ttl_sec', 'status', 'master_ips', 'axfr_ips');

	public static function validateSoa($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'soa_email' => array('required', 'email'),
			'refresh_sec' => array('integer'),
			'retry_sec' => array('integer'),
			'expire_sec' => array('integer'),
			'ttl_sec' => array('integer'),
			'status' => array('in:0,1,2'),
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public function save(array $options = array())
	{
		return App::make('linode.domain')->update($this->domainid, $this->attributes);
	}

	public static function updateSoa($id, $input)
	{
		$soa = new Soa($input);
		$soa->domainid = $id;
		try
		{
			$soa->save();
			\Linode::refreshLinodeDomain($id);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\SoaController@update')
				->with('error', true)
				->with('reason', 'soa.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($id))
			->with('success', "SOA record for domain {$id} updated on Linode");

	}
}

?>
