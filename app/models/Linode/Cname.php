<?php namespace Linode;
/**
 *
 */

use App;
use Redirect;
use Resource;
use Validator;
use Hampel\Linode\Service\LinodeException;

class Cname extends \Eloquent
{
	protected $fillable = array('domainid', 'resourceid', 'name', 'target', 'ttl_sec');

	public function save(array $options = array())
	{
		if ($this->resourceid > 0)
		{
			return App::make('linode.domain.resource')->updateCname($this->domainid, $this->resourceid, $this->name, $this->target, $this->ttl_sec);
		}
		else
		{
			return App::make('linode.domain.resource')->createCname($this->domainid, $this->name, $this->target);
		}
	}

	public function delete()
	{
		return App::make('linode.domain.resource')->delete($this->domainid, $this->resourceid);
	}

	public static function getRecord($linodeid, $resourceid)
	{
		return Resource::getResource($linodeid, $resourceid);
	}

	public static function getAllRecords($linodeid)
	{
		return Resource::getResourcesOfType('CNAME', $linodeid);
	}

	public static function validateCname($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'name' => array('required'),
			'target' => array('required', 'domain'),
			'ttl_sec' => array('integer', 'min:0')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function storeCname($linodeid, $input)
	{
		$cname = new Cname($input);
		$cname->domainid = $linodeid;
		try
		{
			$cname->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\CnameController@create')
				->with('error', true)
				->with('reason', 'cname.create.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "CNAME record for domain {$linodeid} created on Linode");
	}

	public static function updateCname($linodeid, $resourceid, $input)
	{
		$cname = new Cname($input);
		$cname->domainid = $linodeid;
		$cname->resourceid = $resourceid;
		try
		{
			$cname->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\CnameController@edit')
				->with('error', true)
				->with('reason', 'cname.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "CNAME record for domain {$linodeid} updated on Linode");
	}

	public static function deleteRecord($linodeid, $resourceid, $resource)
	{
		$cname = new Cname($resource->get());

		try
		{
			$cname->delete();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not delete CNAME record {$resourceid}: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return Redirect::action('Linode\CnameController@delete', array($linodeid, $resourceid))
				->with('error', true)
				->with('reason', 'cname.delete.failure')
				->with('error_data', array('message' => $e->getMessage()));
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "CNAME record successfully deleted");
	}
}

?>
