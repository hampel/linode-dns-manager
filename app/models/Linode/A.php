<?php namespace Linode;
/**
 *
 */

use App;
use Redirect;
use Resource;
use Validator;
use \Hampel\Linode\Service\LinodeException;

class A extends \Eloquent
{
	protected $fillable = array('domainid', 'resourceid', 'name', 'target', 'ttl_sec');

	public function save(array $options = array())
	{
		if ($this->resourceid > 0)
		{
			return App::make('linode.domain.resource')->updateA($this->domainid, $this->resourceid, $this->name, $this->target, $this->ttl_sec);
		}
		else
		{
			return App::make('linode.domain.resource')->createA($this->domainid, $this->name, $this->target);
		}
	}

	public function delete()
	{
		return App::make('linode.domain.resource')->delete($this->domainid, $this->resourceid);
	}

	public static function getRecord($linodeid, $resourceid)
	{
		return Resource::getResource($linodeid, $resourceid);
	}

	public static function getAllRecords($linodeid)
	{
		return Resource::getResourcesOfType('A', $linodeid);
	}

	public static function validateA($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'target' => array('required', 'ipv4_public'),
			'ttl_sec' => array('integer', 'min:0')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function storeA($linodeid, $input)
	{
		$a = new A($input);
		$a->domainid = $linodeid;
		try
		{
			$a->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\AController@create')
				->with('error', true)
				->with('reason', 'a.create.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "A record for domain {$linodeid} created on Linode");
	}

	public static function updateA($linodeid, $resourceid, $input)
	{
		$a = new A($input);
		$a->domainid = $linodeid;
		$a->resourceid = $resourceid;
		try
		{
			$a->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\AController@edit')
				->with('error', true)
				->with('reason', 'a.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "A record for domain {$linodeid} updated on Linode");
	}

	public static function deleteRecord($linodeid, $resourceid, $resource)
	{
		$a = new A($resource->get());

		try
		{
			$a->delete();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not delete A record {$resourceid}: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return Redirect::action('Linode\AController@delete', array($linodeid, $resourceid))
				->with('error', true)
				->with('reason', 'a.delete.failure')
				->with('error_data', array('message' => $e->getMessage()));
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "A record successfully deleted");
	}
}

?>
