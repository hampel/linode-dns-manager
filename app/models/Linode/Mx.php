<?php namespace Linode;
/**
 * 
 */

use App;
use Redirect;
use Resource;
use Validator;
use Hampel\Linode\Service\LinodeException;

class Mx extends \Eloquent
{
	protected $fillable = array('domainid', 'resourceid', 'target', 'priority', 'name', 'ttl_sec');

	public function save(array $options = array())
	{
		if ($this->resourceid > 0)
		{
			return App::make('linode.domain.resource')->updateMx($this->domainid, $this->resourceid, $this->target, $this->priority, $this->name, $this->ttl_sec);
		}
		else
		{
			return App::make('linode.domain.resource')->createMx($this->domainid, $this->target, $this->priority, $this->name);
		}
	}

	public function delete()
	{
		return App::make('linode.domain.resource')->delete($this->domainid, $this->resourceid);
	}

	public static function getRecord($linodeid, $resourceid)
	{
		return Resource::getResource($linodeid, $resourceid);
	}

	public static function getAllRecords($linodeid)
	{
		return Resource::getResourcesOfType('MX', $linodeid);
	}

	public static function validateMx($input)
	{
		// Declare the rules for the form validation.
		$rules = array(
			'target' => array('required', 'domain'),
			'priority' => array('integer', 'between:0,255'),
			'ttl_sec' => array('integer', 'min:0')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function storeMx($linodeid, $input)
	{
		$mx = new Mx($input);
		$mx->domainid = $linodeid;
		try
		{
			$mx->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\MxController@create')
				->with('error', true)
				->with('reason', 'mx.create.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "MX record for domain {$linodeid} created on Linode");
	}

	public static function updateMx($linodeid, $resourceid, $input)
	{
		$mx = new Mx($input);
		$mx->domainid = $linodeid;
		$mx->resourceid = $resourceid;
		try
		{
			$mx->save();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			return Redirect::action('Linode\MxController@edit')
				->with('error', true)
				->with('reason', 'mx.update.failure')
				->with('error_data', array('message' => $e->getMessage()))
				->withInput($input);
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "MX record for domain {$linodeid} updated on Linode");
	}

	public static function deleteRecord($linodeid, $resourceid, $resource)
	{
		$mx = new Mx($resource->get());

		try
		{
			$mx->delete();
			Resource::refreshResourceCache($linodeid);
		}
		catch (LinodeException $e)
		{
			Log::error("Could not delete MX record {$resourceid}: " . $e->getMessage(), array('error_code' => $e->getCode()));
			return Redirect::action('Linode\MxController@delete', array($linodeid, $resourceid))
				->with('error', true)
				->with('reason', 'mx.delete.failure')
				->with('error_data', array('message' => $e->getMessage()));
		}

		return Redirect::action('LinodeController@show', array($linodeid))
			->with('success', "MX record successfully deleted");
	}
}

?>
