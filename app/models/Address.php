<?php
/**
 * 
 */

class Address extends Eloquent
{
	protected $fillable = array('address', 'name', 'type');
	public $timestamps = false;

	public static function validateCreateAddress($input)
	{
		if (isset($input['type']) AND $input['type'] == 'ipv4') $address_validation = 'ipv4_public';
		else $address_validation = 'ipv6_public';

		// Declare the rules for the form validation.
		$rules = array(
			'address' => array('required', $address_validation),
			'name' => array('required'),
			'type' => array('required', 'in:ipv4,ipv6')
		);

		// Validate the inputs.
		return Validator::make($input, $rules);
	}

	public static function getTypes()
	{
		return array(
			'ipv4' => "IPv4",
			'ipv6' => "IPv6"
		);
	}

	public static function getAddressMap()
	{
		$map = array();
		$addresses = Address::all();
		foreach ($addresses as $address)
		{
			$map[$address->address] = $address->name;
		}

		return $map;
	}

	public static function getAddressList($type = "ipv4")
	{
		$map = array();
		$addresses = Address::where('type', '=', $type)->get();
		foreach ($addresses as $address)
		{
			$map[$address->address] = "{$address->name} ({$address->address})";
		}

		return $map;
	}

	public static function mapAddress($map, $address)
	{
		if (array_key_exists($address, $map)) return "{$map[$address]} ({$address})";
		else return $address;
	}

	public static function getDescription($map, $address)
	{
		if (array_key_exists($address, $map)) return $map[$address];
		else return "";
	}

}

?>
