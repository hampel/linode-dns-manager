<?php

use Illuminate\Database\Migrations\Migration;

class AddDomainsBaseField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('domains', function($table)
		{
			$table->string('base', 64)->after('name')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('domains', function($table)
		{
			$table->dropColumn('base');
		});
	}

}