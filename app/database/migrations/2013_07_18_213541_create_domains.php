<?php

use Illuminate\Database\Migrations\Migration;

class CreateDomains extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('domains', function($table)
		{
			$table->increments('id');
			$table->string('name', 64)->unique();
			$table->integer('parent')->unsigned()->index();
			$table->integer('linodeid')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('domains');
	}

}