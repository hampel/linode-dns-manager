@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $linode->getDomainName() }}</h1>

			@if ($mx->resourceid > 0)
				<h2>Edit MX Record</h2>
				{{ Form::model($mx, array('route' => array('linode.mx.update', 'linode' => $mx->domainid, 'mx' => $mx->resourceid), 'method' => 'PUT', 'class' => 'custom')) }}
			@else
				<h2>Create MX Record</h2>
				{{ Form::model($mx, array('route' => array('linode.mx.store', 'linode' => $mx->domainid), 'method' => 'POST', 'class' => 'custom')) }}
			@endif

			<!-- Mail Server / Target -->
			<div class="row collapse {{{ $errors->has('target') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('target', 'Mail Server') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('target', $mx->target) }}
					{{ $errors->first('target', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;Eg: mail.example.com</div>
			</div>

			<!-- Priority -->
			<div class="row collapse {{{ $errors->has('priority') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('priority', 'Priority') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('priority', $mx->priority) }}
					{{ $errors->first('priority', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;0-255 (optional)</div>
			</div>

			<!-- Subdomain / Name -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Subdomain') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('name', $mx->name) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;(optional)</div>
			</div>

			@if ($mx->resourceid > 0)
				<!-- TTL -->
				<div class="row collapse {{{ $errors->has('ttl_sec') ? 'error' : '' }}}">
					<div class="large-2 columns">
						<span class="prefix radius">{{ Form::label('ttl_sec', 'TTL') }}</span>
					</div>

					<div class="large-3 columns">
						{{ Form::select('ttl_sec', $times, $mx->ttl_sec) }}
						{{ $errors->first('ttl_sec', '<small>:message</small>') }}
					</div>
					<div class="large-7 columns">&nbsp;</div>
				</div>
			@endif

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $mx->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>
@stop
