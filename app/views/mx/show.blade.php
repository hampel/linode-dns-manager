<div class="resource-buttons">
	<ul class="button-group radius">
		<li><a href="{{ action('Linode\MxController@create', array('linode' => $domain->linodeid)) }}" title="Create MX Record" class="button secondary small"><i class="icon-plus icon-large"></i>Add MX</a></li>
	</ul>
</div>
<h3>MX Records</h3>

<table class="dns-records">
	<thead>
		<tr>
			<th>Mail Server</th>
			<th>Pref</th>
			<th>Subdomain</th>
			<th>TTL</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		@if (!empty($mx))
			@foreach ($mx as $resourceid => $record)
				<tr>
					<td class="wrappable">{{ $record->getHostname() }}</td>
					<td class="text-center">{{ $record->getPriority() }}</td>
					<td>{{ $record->getSubdomain() }}</td>
					<td class="text-center">{{ $record->getTtl() == 0 ? "Default" : $record->getTtl() }}</td>
					<td class="options">
						<ul>
							<li><a href="{{ action('Linode\MxController@edit', array('mx' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Edit MX Record"><i class="icon-pencil icon-large"></i></a></li>
							<li><a href="{{ action('Linode\MxController@delete', array('mx' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Remove MX Record"><i class="icon-remove icon-large"></i></a></li>
						</ul>
					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>