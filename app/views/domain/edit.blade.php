@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">

			@if ($domain->id > 0)
				<h2>Edit Domain</h2>
				{{ Form::model($domain, array('route' => array('domain.update', 'domain' => $domain->id), 'method' => 'PUT', 'class' => 'custom')) }}
			@else
				<h2>Create Domain</h2>
				{{ Form::model($domain, array('route' => array('domain.store'), 'method' => 'POST', 'class' => 'custom')) }}
			@endif

			<!-- Name -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Domain Name') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('name', $domain->name, isset($domain->name) ? array('readonly') : null) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Parent -->
			<div class="row collapse {{{ $errors->has('parent') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('parent', 'Parent Domain') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('parent', $parents, $domain->parent) }}
					{{ $errors->first('parent', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Linode ID -->
			@if (!is_null($domain->linodeid) AND !$orphaned)
			<div class="row collapse {{{ $errors->has('linodeid') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('linodeid', 'Linode ID') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('linodeid', $domain->linodeid, array('readonly')) }}
					{{ $errors->first('linodeid', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>
			@endif

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					@if ($domain->id > 0)
						<a href="{{ action('DomainController@show', array('domain' => $domain->id)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
					@else
						<a href="{{ action('DomainController@index') }}" title="Cancel" class="button secondary medium radius">Cancel</a>
					@endif
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
@stop
