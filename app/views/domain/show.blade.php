@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-8 columns">

			<h2>{{ $domain->name }}</h2>

			<p class="small"><i class="icon-reply"></i>{{ link_to_action('DomainController@index', 'Back to Domain Index') }}</p>

			@if (is_null($linode))
				<p>This domain does not have an entry in Linode. {{ link_to_action('DomainController@edit', 'Create', array('domain' => $domain->id)) }}</p>
			@else
				@include('soa.show')
				@include('mx.show')
				@include('a.show')
				@include('cname.show')
				@include('txt.show')
			@endif

		</div>

		<div class="large-4 columns">
			@if ($domain->parent == 0)
				<div class="resource-buttons">
					<ul class="button-group radius">
						<li><a href="{{ action('DomainController@create', array('parent' => $domain->id)) }}" title="Create Alias" class="button secondary small"><i class="icon-plus icon-large"></i>Add Alias</a></li>
					</ul>
				</div>
				<h3>Domain Aliases</h3>
				<table class="dns-records">
					<thead>
					<tr>
						<th>Domain</th>
						<th>Options</th>
					</tr>
					</thead>
					<tbody>
					@foreach($children as $child)
					<tr>
						<td>{{ link_to_action('DomainController@show', $child->name, array($child->id)) }}</td>
						<td class="options">
							<ul>
								<li><a href="{{ action('DomainController@edit', array('domain' => $child->id)) }}" title="Edit Domain Record"><i class="icon-pencil icon-large"></i></a></li>
								<li><a href="{{ URL::action('DomainController@delete', array('id' => $child->id)) }}" title="Delete Domain"><i class="icon-remove icon-large"></i></a></li>
							</ul>
						</td>
					</tr>
					@endforeach
					</tbody>
				</table>
			@else
				<h3>Parent Domain</h3>
				<table class="dns-records">
					<thead>
					<tr>
						<th>Domain</th>
						<th>Options</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td class="wrappable">{{ link_to_action('DomainController@show', $parent->name, array($parent->id)) }}</td>
						<td class="options">
							<ul>
								<li><a href="{{ action('DomainController@edit', array('domain' => $parent->id)) }}" title="Edit Domain Record"><i class="icon-pencil icon-large"></i></a></li>
							</ul>
						</td>
					</tr>
					</tbody>
				</table>

				<div class="resource-buttons">
					<ul class="button-group radius">
						<li><a href="{{ action('DomainController@create', array('parent' => $domain->parent)) }}" title="Create Alias" class="button secondary small"><i class="icon-plus icon-large"></i>Add Alias</a></li>
					</ul>
				</div>
				<h3>Other Aliases</h3>
				<table class="dns-records">
					<thead>
					<tr>
						<th>Domain</th>
						<th>Options</th>
					</tr>
					</thead>
					<tbody>
					@foreach($siblings as $sibling)
					<tr>
						<td class="wrappable">{{ link_to_action('DomainController@show', $sibling->name, array($sibling->id)) }}</td>
						<td class="options">
							<ul>
								<li><a href="{{ action('DomainController@edit', array('domain' => $sibling->id)) }}" title="Edit Domain Record"><i class="icon-pencil icon-large"></i></a></li>
								<li><a href="{{ URL::action('DomainController@delete', array('id' => $sibling->id)) }}" title="Delete Domain"><i class="icon-remove icon-large"></i></a></li>
							</ul>
						</td>
					</tr>
					@endforeach
					</tbody>
				</table>
			@endif

			@if (!empty($aliases))
				<h3>Add Aliases</h3>
				{{ Form::open(array('action' => array('DomainController@aliases', 'id' => $domain->id))) }}
				<table class="dns-records">
					<thead>
					<tr>
						<th>Domain Base: {{ $base }}</th>
						<th>Options</th>
					</tr>
					</thead>
					<tbody>
					@foreach($aliases as $alias)
					<tr>
						<td class="wrappable">{{ $base . $alias }}{{ in_array($alias, $unmanaged) ? " *" : "" }}</td>
						<td class="options">
							<ul>
								<li><a href="{{ action('DomainController@create', array('name' => $base . $alias, 'parent' => $domain->parent > 0 ? $domain->parent : $domain->id)) }}" title="Add Domain Alias"><i class="icon-plus icon-large"></i></a></li>
								<li class="alias">{{ Form::checkbox('tld[]', $alias, false, array('class' => 'alias')) }}</li>
							</ul>
						</td>
					</tr>
					@endforeach
					<tr>
						<td>
							@if (empty($unmanaged))
								&nbsp;
							@else
								* = unmanaged domains
							@endif
						</td>
						<td>{{ Form::submit('Add', array('class' => 'button small radius right', 'title' => 'Add Selected Aliases', 'id' => 'alias-button')) }}</td>
					</tr>
					</tbody>
				</table>
				{{ Form::close() }}
			@endif
		</div>
	</div>
@stop
