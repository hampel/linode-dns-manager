@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-8 columns">

			<div class="index-buttons">
				<ul class="button-group radius right">
					<li><a href="{{ URL::action('DomainController@create') }}"" title="Create New Domain" class="button secondary small"><i class="icon-plus icon-large"></i>Add Domain</a></li>
					<li><a href="{{ URL::route('map-cache-refresh') }}"" title="Refresh Domain List" class="button secondary small"><i class="icon-refresh icon-large"></i>Refresh Domains</a></li>
					<li><a href="{{ URL::action('AddressController@index') }}"" title="Manage Addresses" class="button secondary small"><i class="icon-globe icon-large"></i>Manage Addresses</a></li>
				</ul>
			</div>

			<h2>Domains</h2>
			<table class="dns-records">
				<thead>
				<tr>
					<th>Domain</th>
					<th>Type</th>
					<th>Aliases</th>
					<th>Options</th>
				</tr>
				</thead>
				<tbody>
				@foreach($parents as $name => $parent)
					<tr>
						<td class="wrappable">{{ link_to_action('DomainController@show', $name, array($parent->id)) }}</td>
						<td class="text-center">{{ $parent->type }}</td>
						<td class="text-center">{{ $aliases[$parent->id] > 0 ? $aliases[$parent->id] : "" }}</td>
						<td class="options">
							<ul>
								<li><a href="{{ action('DomainController@edit', array('domain' => $parent->id)) }}" title="Edit Domain Record"><i class="icon-pencil icon-large"></i></a></li>
								<li><a href="{{ action('DomainController@delete', array('id' => $parent->id)) }}" title="Delete Domain"><i class="icon-remove icon-large"></i></a></li>
							</ul>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>

		<div class="large-4 columns">
			@if(!empty($orphaned))
				<h3>Orphaned Domains</h3>
				<p>Domains where the Linode entry has been deleted, but we still have an entry in the database.</p>
				<table class="dns-records">
					<thead>
					<tr>
						<th>Domain</th>
						<th>Options</th>
					</tr>
					</thead>
					<tbody>
					@foreach($orphaned as $name => $id)
						<tr>
							<td class="wrappable">{{ link_to_action('DomainController@show', $name, array('domain' => $id)) }}</td>
							<td class="options">
								<ul>
									<li><a href="{{ action('DomainController@edit', array('domain' => $id)) }}" title="Edit Domain Record"><i class="icon-pencil icon-large"></i></a></li>
									<li><a href="{{ URL::action('DomainController@delete', array('id' => $id)) }}" title="Remove Domain from Database"><i class="icon-remove icon-large"></i></a></li>
								</ul>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif

			@if(!empty($unmanaged))
				<h3>Unmanaged Domains</h3>
				<p>Domains on Linode not yet in our database.</p>
				<table class="dns-records">
					<thead>
					<tr>
						<th>Domain</th>
						<th>Options</th>
					</tr>
					</thead>
					<tbody>
					@foreach($unmanaged as $name => $linodeid)
						<tr>
							<td class="wrappable">{{ $name }}</td>
							<td class="options">
								<ul>
									<li><a href="{{ URL::action('DomainController@create', array('linodeid' => $linodeid)) }}" title="Add Domain Record"><i class="icon-plus icon-large"></i></a></li>
									<li><a href="{{ URL::action('LinodeController@delete', array('id' => $linodeid)) }}" title="Remove Domain from Linode"><i class="icon-remove icon-large"></i></a></li>
								</ul>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif

		</div>
	</div>
@stop
