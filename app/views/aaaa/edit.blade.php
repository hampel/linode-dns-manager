@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $linode->getDomainName() }}</h1>

			<div class="index-buttons">
				<ul class="button-group radius right">
					<li><a href="{{ URL::action('AddressController@create') }}"" title="Add Address" class="button secondary small"><i class="icon-plus icon-large"></i>Add Address</a></li>
				</ul>
			</div>

			@if ($aaaa->resourceid > 0)
				<h2>Edit AAAA Record</h2>
				{{ Form::model($aaaa, array('route' => array('linode.aaaa.update', 'linode' => $aaaa->domainid, 'aaaa' => $aaaa->resourceid), 'method' => 'PUT', 'class' => 'custom')) }}
			@else
				<h2>Create AAAA Record</h2>
				{{ Form::model($aaaa, array('route' => array('linode.aaaa.store', 'linode' => $aaaa->domainid), 'method' => 'POST', 'class' => 'custom')) }}
			@endif

			<!-- Hostname -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Hostname') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('name', $aaaa->name) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;Eg: ftp</div>
			</div>

			<!-- IP Address / Target -->
			<div class="row collapse {{{ $errors->has('target') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('target', 'IPv6 Address') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('target', $addresses, $aaaa->target) }}
					{{ $errors->first('target', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>


			@if ($aaaa->resourceid > 0)
				<!-- TTL -->
				<div class="row collapse {{{ $errors->has('ttl_sec') ? 'error' : '' }}}">
					<div class="large-2 columns">
						<span class="prefix radius">{{ Form::label('ttl_sec', 'TTL') }}</span>
					</div>

					<div class="large-3 columns">
						{{ Form::select('ttl_sec', $times, $aaaa->ttl_sec) }}
						{{ $errors->first('ttl_sec', '<small>:message</small>') }}
					</div>
					<div class="large-7 columns">&nbsp;</div>
				</div>
			@endif

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $aaaa->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>
@stop
