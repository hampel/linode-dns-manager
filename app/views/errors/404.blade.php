@extends('layouts.base')

{{-- Login page layout --}}

@section('body')
<div class="row">
	<div class="large-10 large-centered columns">

		<div data-alert class="alert-box secondary radius">
			<h3>Error: {{{ $message }}}</h3>
		</div>

	</div>
</div>
@stop
