@extends('layouts.base')

{{-- Create new domain layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">

			<h2>Create a new domain</h2>
			{{ Form::open(array('action' => 'LinodeController@store', 'class' => 'custom')) }}

			<!-- Name -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Domain Name') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('name', Input::old('name')) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Parent -->
			<div class="row collapse {{{ $errors->has('parent') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('parent', 'Parent Domain') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('parent', $parents) }}
					{{ $errors->first('parent', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Save button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
@stop
