<div class="resource-buttons">
	<ul class="button-group radius">
		<li><a href="{{ action('Linode\CnameController@create', array('linode' => $domain->linodeid)) }}" title="Create CNAME Record" class="button secondary small"><i class="icon-plus icon-large"></i>Add CNAME</a></li>
	</ul>
</div>
<h3>CNAME Records</h3>

<table class="dns-records">
	<thead>
		<tr>
			<th>Hostname</th>
			<th>Aliases to</th>
			<th>TTL</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		@if (!empty($cname))
			@foreach ($cname as $resourceid => $record)
				<tr>
					<td>{{ $record->getHostname() }}</td>
					<td class="wrappable">{{ $record->getTarget() }}</td>
					<td class="text-center">{{ $record->getTtl() == 0 ? "Default" : $record->getTtl() }}</td>
					<td class="options">
						<ul>
							<li><a href="{{ action('Linode\CnameController@edit', array('cname' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Edit CNAME Record"><i class="icon-pencil icon-large"></i></a></li>
							<li><a href="{{ action('Linode\CnameController@delete', array('cname' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Remove CNAME Record"><i class="icon-remove icon-large"></i></a></li>
						</ul>
					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>