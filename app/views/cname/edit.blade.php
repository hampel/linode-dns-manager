@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $linode->getDomainName() }}</h1>

			@if ($cname->resourceid > 0)
				<h2>Edit CNAME Record</h2>
				{{ Form::model($cname, array('route' => array('linode.cname.update', 'linode' => $cname->domainid, 'cname' => $cname->resourceid), 'method' => 'PUT', 'class' => 'custom')) }}
			@else
				<h2>Create CNAME Record</h2>
				{{ Form::model($cname, array('route' => array('linode.cname.store', 'linode' => $cname->domainid), 'method' => 'POST', 'class' => 'custom')) }}
			@endif

			<!-- Hostname -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Hostname') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('name', $cname->name) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;Eg: mail</div>
			</div>

			<!-- Target -->
			<div class="row collapse {{{ $errors->has('target') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('target', 'Aliases to') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('target', $cname->target) }}
					{{ $errors->first('target', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;Eg: mail.example.com</div>
			</div>

			@if ($cname->resourceid > 0)
				<!-- TTL -->
				<div class="row collapse {{{ $errors->has('ttl_sec') ? 'error' : '' }}}">
					<div class="large-2 columns">
						<span class="prefix radius">{{ Form::label('ttl_sec', 'TTL') }}</span>
					</div>

					<div class="large-3 columns">
						{{ Form::select('ttl_sec', $times, $cname->ttl_sec) }}
						{{ $errors->first('ttl_sec', '<small>:message</small>') }}
					</div>
					<div class="large-7 columns">&nbsp;</div>
				</div>
			@endif

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $cname->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>
@stop
