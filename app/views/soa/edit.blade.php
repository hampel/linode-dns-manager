@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $soa->domain }}</h1>

			<h2>Edit Domain SOA Record</h2>
			{{ Form::model($soa, array('route' => array('soa.update', 'soa' => $soa->domainid), 'method' => 'PUT', 'class' => 'custom')) }}

			<!-- SOA Email -->
			<div class="row collapse {{{ $errors->has('soa_email') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('soa_email', 'SOA Email') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('soa_email', $soa->soa_email) }}
					{{ $errors->first('soa_email', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			{{--
			<!-- Description -->
			<div class="row collapse {{{ $errors->has('description') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('description', 'Description') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('description', $soa->description) }}
					{{ $errors->first('description', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>
			--}}

			<!-- Status -->
			<div class="row collapse {{{ $errors->has('status') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('status', 'Status') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('status', $status, $soa->status) }}
					{{ $errors->first('status', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- TTL -->
			<div class="row collapse {{{ $errors->has('ttl_sec') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('ttl_sec', 'Default TTL') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('ttl_sec', $times, $soa->ttl_sec) }}
					{{ $errors->first('ttl_sec', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Refresh -->
			<div class="row collapse {{{ $errors->has('refresh_sec') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('refresh_sec', 'Refresh Rate') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('refresh_sec', $times, $soa->refresh_sec) }}
					{{ $errors->first('refresh_sec', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Retry -->
			<div class="row collapse {{{ $errors->has('retry_sec') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('retry_sec', 'Retry Rate') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('retry_sec', $times, $soa->retry_sec) }}
					{{ $errors->first('retry_sec', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Expire -->
			<div class="row collapse {{{ $errors->has('expire_sec') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('expire_sec', 'Expire Rate') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('expire_sec', $times, $soa->expire_sec) }}
					{{ $errors->first('expire_sec', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $soa->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>
@stop
