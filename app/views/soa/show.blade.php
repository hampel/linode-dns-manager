<div class="resource-buttons">
	<ul class="button-group radius">
		<li><a href="{{ action('DomainController@edit', array('domain' => $domain->id)) }}" title="Edit Domain" class="button secondary small"><i class="icon-pencil icon-large"></i>Edit Domain</a></li>
		<li><a href="{{ URL::route('domain-cache-refresh', array('id' => $domain->linodeid)) }}"" class="button secondary small"><i class="icon-refresh icon-large"></i>Refresh Domain</a></li>
		<li><a href="{{ action('DomainController@delete', array('id' => $domain->id)) }}" title="Delete Domain" class="button secondary small"><i class="icon-remove icon-large"></i>Delete Domain</a></li>
	</ul>
</div>
<h3>SOA Record</h3>

<table class="dns-records">
	<thead>
		<tr>
			<th>Email</th>
			<th>Default TTL</th>
			<th>Refresh Rate</th>
			<th>Retry Rate</th>
			<th>Expire Time</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="text-center wrappable">{{ $linode['soa_email'] }}</td>
			<td class="text-center">{{ $linode['ttl_sec'] == 0 ? "Default" : $linode['ttl_sec'] }}</td>
			<td class="text-center">{{ $linode['refresh_sec'] == 0 ? "Default" : $linode['refresh_sec'] }}</td>
			<td class="text-center">{{ $linode['retry_sec'] == 0 ? "Default" : $linode['retry_sec'] }}</td>
			<td class="text-center">{{ $linode['expire_sec'] == 0 ? "Default" : $linode['expire_sec'] }}</td>
			<td class="options">
				<ul>
					<li><a href="{{ action('Linode\SoaController@edit', array('soa' => $domain->linodeid)) }}" title="Edit SOA Record"><i class="icon-pencil icon-large"></i></a></li>
				</ul>
			</td>
		</tr>
	</tbody>
</table>