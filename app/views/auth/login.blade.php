@extends('layouts.base')

{{-- Login page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">

			<h2>Login into your account</h2>
			{{ Form::open(array('url' => 'login')) }}

			<!-- Name -->
			<div class="row collapse {{{ $errors->has('username') ? 'error' : '' }}}">
				<div class="large-1 columns">
					<span class="prefix radius">{{ Form::label('username', 'Username') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('username', Input::old('username')) }}
					{{ $errors->first('username', '<small>:message</small>') }}
				</div>
				<div class="large-8 columns">&nbsp;</div>
			</div>

			<!-- Password -->
			<div class="row collapse {{{ $errors->has('password') ? 'error' : '' }}}">
				<div class="large-1 columns">
					<span class="prefix radius">{{ Form::label('password', 'Password') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::password('password') }}
					{{ $errors->first('password', '<small>:message</small>') }}
				</div>
				<div class="large-8 columns">&nbsp;</div>
			</div>

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Login', array('class' => 'button medium radius')) }} &raquo; <small>{{ link_to_route('lost-password', 'Forgotten your password?') }}</small>
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
@stop
