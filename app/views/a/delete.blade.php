@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">

			<h2>Delete A Record</h2>
			{{ Form::model($a, array('route' => array('linode.a.destroy', 'linode' => $a->domainid, 'a' => $a->resourceid), 'method' => 'DELETE', 'class' => 'custom')) }}

			<!-- Hostname -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Hostname') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('name', $a->name, array('readonly')) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<p>Are you sure you wish to delete this A record?</p>

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Delete', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $a->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
@stop
