<div class="resource-buttons">
	<ul class="button-group radius">
		<li><a href="{{ action('Linode\AController@create', array('linode' => $domain->linodeid)) }}" title="Create A Record" class="button secondary small"><i class="icon-plus icon-large"></i>Add A</a></li>
		<li><a href="{{ action('Linode\AaaaController@create', array('linode' => $domain->linodeid)) }}" title="Create AAAA Record" class="button secondary small"><i class="icon-plus icon-large"></i>Add AAAA</a></li>
	</ul>
</div>
<h3>A/AAAA Records</h3>

<table class="dns-records">
	<thead>
		<tr>
			<th>Hostname</th>
			<th>IP Address</th>
			{{-- <th>Description</th> --}}
			<th>TTL</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		@if (!empty($a))
			@foreach ($a as $resourceid => $record)
				<tr>
					<td>{{ $record->getHostname() }}</td>
					<td class="wrappable">{{ Address::mapAddress($map, $record->getIp()) }}</td>
					{{-- <td class="text-center wrappable">{{ Address::getDescription($map, $record->getIp()) }}</td> --}}
					<td class="text-center">{{ $record->getTtl() == 0 ? "Default" : $record->getTtl() }}</td>
					<td class="options">
						<ul>
							<li><a href="{{ action('Linode\AController@edit', array('a' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Edit A Record"><i class="icon-pencil icon-large"></i></a></li>
							<li><a href="{{ action('Linode\AController@delete', array('a' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Remove A Record"><i class="icon-remove icon-large"></i></a></li>
						</ul>
					</td>
				</tr>
			@endforeach
		@endif
		@if (!empty($aaaa))
			@foreach ($aaaa as $resourceid => $record)
			<tr>
				<td>{{ $record->getHostname() }}</td>
				<td class="wrappable">{{ Address::mapAddress($map, $record->getIp()) }}</td>
				{{-- <td class="text-center wrappable">{{ Address::getDescription($map, $record->getIp()) }}</td> --}}
				<td class="text-center">{{ $record->getTtl() == 0 ? "Default" : $record->getTtl() }}</td>
				<td class="options">
					<ul>
						<li><a href="{{ action('Linode\AaaaController@edit', array('aaaa' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Edit AAAA Record"><i class="icon-pencil icon-large"></i></a></li>
						<li><a href="{{ action('Linode\AaaaController@delete', array('aaaa' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Remove AAAA Record"><i class="icon-remove icon-large"></i></a></li>
					</ul>
				</td>
			</tr>
			@endforeach
		@endif
	</tbody>
</table>