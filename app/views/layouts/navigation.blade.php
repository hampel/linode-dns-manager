<div class="contain-to-grid">
	<nav class="top-bar">
		<ul class="title-area">
			<!-- Title Area -->
			<li class="name">

			</li>

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="left">
				<li>{{ link_to_route('home', 'Home') }}</li>
				<li>{{ link_to_action('LinodeController@create', 'New Domain') }}</li>
			</ul>

			<ul class="right">
				<li class="has-dropdown"><a href="#">Admin</a>
					<ul class="dropdown">
						@if ( Auth::guest() )
							<li><label>Not logged in</label></li>
							<li><a href="{{ URL::route('login') }}"><i class="icon-signin"></i>Login</a></li>
						@else
							<li><label>Logged in as: {{ Auth::user()->id }}</label></li>
							<li><a href="{{ URL::route('change-email') }}"><i class="icon-envelope"></i>Change Email</a></li>
							<li><a href="{{ URL::route('change-password') }}"><i class="icon-key"></i>Change Password</a></li>
							<li><a href="{{ URL::route('logout') }}"><i class="icon-signout"></i>Logout</a></li>
						@endif
						<li class="dropdown-bottom">&nbsp;</li>
					</ul>
				</li>
			</ul>
		</section>
	</nav>
</div>