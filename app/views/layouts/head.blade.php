<head>
	<meta charset=UTF-8" />
	<meta name="viewport" content="width=device-width" />
	<title>Hampel Group DNS Manager</title>

	<link rel="icon" type="image/png" href="{{ URL::asset('images/icon.png') }}">

	<link href="{{ URL::asset('assets/css/app.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/css/font-awesome/font-awesome.css') }}" rel="stylesheet" type="text/css" />
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro%7CDroid+Sans+Mono%7COpen+Sans%3A400italic%2C700italic%2C400%2C700&#038' type='text/css' media='all' />
	<script src="{{ URL::asset('assets/js/vendor/custom.modernizr.js') }}"></script>


	<meta name="robots" content="noindex,nofollow" />
</head>