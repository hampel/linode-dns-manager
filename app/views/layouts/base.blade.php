<?php
/**
 * Base layout
 */
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
@include('layouts.head')
<body>
	@include('layouts.header')

	@section('alerts')
		@include('layouts.alerts')
	@show

	@yield('body')

	@include('layouts.footer')

	@include('layouts.foundation')
</body>
</html>