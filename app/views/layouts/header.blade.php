<header>
	<div class="row">
		<div class="large-12 columns">
			<h1>
				{{ link_to_route('home', 'Linode DNS Manager') }}
			</h1>

			<div class="menu-button">
				<a href="#" data-dropdown="admin-dropdown" class="button dropdown small radius">Admin</a><br>
				<ul id="admin-dropdown" data-dropdown-content class="f-dropdown">
					@if ( Auth::guest() )
						<li><label>Not logged in</label></li>
						<li><a href="{{ URL::route('login') }}"><i class="icon-signin"></i>Login</a></li>
					@else
						<li><label>Logged in as: {{ Auth::user()->id }}</label></li>
						<li><a href="{{ URL::route('change-email') }}"><i class="icon-envelope"></i>Change Email</a></li>
						<li><a href="{{ URL::route('change-password') }}"><i class="icon-key"></i>Change Password</a></li>
						<li><a href="{{ URL::route('logout') }}"><i class="icon-signout"></i>Logout</a></li>
					@endif
				</ul>
			</div>
		</div>
	</div>
</header>