<script>
	document.write('<script src=' + ('__proto__' in {} ? '/assets/js/vendor/zepto' : '/assets/js/vendor/jquery') + '.js><\/script>')
</script>

<script src="{{ URL::asset('assets/js/foundation/foundation.js') }}"></script>
<script src="{{ URL::asset('assets/js/foundation/foundation.topbar.js') }}"></script>
<script src="{{ URL::asset('assets/js/foundation/foundation.forms.js') }}"></script>
<script src="{{ URL::asset('assets/js/foundation/foundation.section.js') }}"></script>
<script src="{{ URL::asset('assets/js/foundation/foundation.cookie.js') }}"></script>
<script src="{{ URL::asset('assets/js/foundation/foundation.tooltips.js') }}"></script>
<script src="{{ URL::asset('assets/js/foundation/foundation.alerts.js') }}"></script>
<script src="{{ URL::asset('assets/js/foundation/foundation.reveal.js') }}"></script>
<script src="{{ URL::asset('assets/js/foundation/foundation.dropdown.js') }}"></script>

<script>
	$(document).foundation();
</script>