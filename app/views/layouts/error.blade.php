@if (Session::get('error'))
<div class="row">
	<div class="large-12 columns">
		<div data-alert class="alert-box alert radius">
			@if (Session::has('error_data'))
				{{{ Lang::get(Session::get('reason'), Session::get('error_data')) }}}
			@else
				{{{ Lang::get(Session::get('reason')) }}}
			@endif
			<a href="#" class="close">&times;</a>
		</div>
	</div>
</div>
@endif