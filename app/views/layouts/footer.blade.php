<footer>
	<div class="row">
		<div class="large-6 large-centered columns">
			<div id="copyright">Linode DNS Manager was created by <a href="http://hampelgroup.com/" target="_blank">Hampel Group Pty Ltd</a></div>
		</div>
	</div>
</footer>