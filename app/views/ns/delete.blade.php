@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">

			<h2>Delete NS Record</h2>
			{{ Form::model($ns, array('route' => array('linode.ns.destroy', 'linode' => $ns->domainid, 'ns' => $ns->resourceid), 'method' => 'DELETE', 'class' => 'custom')) }}

			<!-- Target -->
			<div class="row collapse {{{ $errors->has('target') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('target', 'Name Server') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('target', $ns->target, array('readonly')) }}
					{{ $errors->first('target', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<p>Are you sure you wish to delete this NS record?</p>

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Delete', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $ns->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
@stop
