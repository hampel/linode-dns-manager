@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $linode->getDomainName() }}</h1>

			@if ($ns->resourceid > 0)
				<h2>Edit NS Record</h2>
				{{ Form::model($ns, array('route' => array('linode.ns.update', 'linode' => $ns->domainid, 'ns' => $ns->resourceid), 'method' => 'PUT', 'class' => 'custom')) }}
			@else
				<h2>Create NS Record</h2>
				{{ Form::model($ns, array('route' => array('linode.ns.store', 'linode' => $ns->domainid), 'method' => 'POST', 'class' => 'custom')) }}
			@endif

			<!-- Name Server / Target -->
			<div class="row collapse {{{ $errors->has('target') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('target', 'Name Server') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('target', $ns->target) }}
					{{ $errors->first('target', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;Eg: mail.example.com</div>
			</div>

			<!-- Subdomain / Name -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Subdomain') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('name', $ns->name) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;(optional)</div>
			</div>

			@if ($ns->resourceid > 0)
				<!-- TTL -->
				<div class="row collapse {{{ $errors->has('ttl_sec') ? 'error' : '' }}}">
					<div class="large-2 columns">
						<span class="prefix radius">{{ Form::label('ttl_sec', 'TTL') }}</span>
					</div>

					<div class="large-3 columns">
						{{ Form::select('ttl_sec', $times, $ns->ttl_sec) }}
						{{ $errors->first('ttl_sec', '<small>:message</small>') }}
					</div>
					<div class="large-7 columns">&nbsp;</div>
				</div>
			@endif

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $ns->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>
@stop
