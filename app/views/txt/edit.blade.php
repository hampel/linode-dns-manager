@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $linode->getDomainName() }}</h1>

			@if ($txt->resourceid > 0)
				<h2>Edit TXT Record</h2>
				{{ Form::model($txt, array('route' => array('linode.txt.update', 'linode' => $txt->domainid, 'txt' => $txt->resourceid), 'method' => 'PUT', 'class' => 'custom')) }}
			@else
				<h2>Create TXT Record</h2>
				{{ Form::model($txt, array('route' => array('linode.txt.store', 'linode' => $txt->domainid), 'method' => 'POST', 'class' => 'custom')) }}
			@endif

			<!-- Name -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Name') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('name', $txt->name) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Value / Target -->
			<div class="row collapse {{{ $errors->has('target') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('target', 'Value') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('target', $txt->target) }}
					{{ $errors->first('target', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			@if ($txt->resourceid > 0)
				<!-- TTL -->
				<div class="row collapse {{{ $errors->has('ttl_sec') ? 'error' : '' }}}">
					<div class="large-2 columns">
						<span class="prefix radius">{{ Form::label('ttl_sec', 'TTL') }}</span>
					</div>

					<div class="large-3 columns">
						{{ Form::select('ttl_sec', $times, $txt->ttl_sec) }}
						{{ $errors->first('ttl_sec', '<small>:message</small>') }}
					</div>
					<div class="large-7 columns">&nbsp;</div>
				</div>
			@endif

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $txt->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>
@stop
