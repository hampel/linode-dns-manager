<div class="resource-buttons">
	<ul class="button-group radius">
		<li><a href="{{ action('Linode\TxtController@create', array('linode' => $domain->linodeid)) }}" title="Create TXT Record" class="button secondary small"><i class="icon-plus icon-large"></i>Add TXT</a></li>
	</ul>
</div>
<h3>TXT Records</h3>

<table class="dns-records">
	<thead>
		<tr>
			<th>Name</th>
			<th>Value</th>
			<th>TTL</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		@if (!empty($txt))
			@foreach ($txt as $resourceid => $record)
				<tr>
					<td class="wrappable min10">{{ $record->getName() }}</td>
					<td class="wrappable">{{ $record->getValue() }}</td>
					<td class="text-center">{{ $record->getTtl() == 0 ? "Default" : $record->getTtl() }}</td>
					<td class="options">
						<ul>
							<li><a href="{{ action('Linode\TxtController@edit', array('txt' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Edit TXT Record"><i class="icon-pencil icon-large"></i></a></li>
							<li><a href="{{ action('Linode\TxtController@delete', array('txt' => $record->getResourceId(), 'linode' => $domain->linodeid)) }}" title="Remove TXT Record"><i class="icon-remove icon-large"></i></a></li>
						</ul>
					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>