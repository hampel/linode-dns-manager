@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">
			@if ($address->id > 0)
				<h2>Edit Address</h2>
				{{ Form::model($address, array('route' => array('address.update', 'address' => $address->id), 'method' => 'PUT', 'class' => 'custom')) }}
			@else
				<h2>Create Address</h2>
				{{ Form::model($address, array('route' => array('address.store'), 'method' => 'POST', 'class' => 'custom')) }}
			@endif

			<!-- Address -->
			<div class="row collapse {{{ $errors->has('address') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('address', 'IP Address') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('address', $address->address) }}
					{{ $errors->first('address', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Name -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Alias / Description') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('name', $address->name) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Type -->
			<div class="row collapse {{{ $errors->has('type') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('type', 'Type') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('type', $types, $address->type) }}
					{{ $errors->first('type', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					<a href="{{ action('AddressController@index') }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>
@stop
