@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">

			<h2>Delete Address</h2>
			{{ Form::model($address, array('route' => array('address.destroy', 'address' => $address->id), 'method' => 'DELETE', 'class' => 'custom')) }}

			<!-- Name -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Name') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('name', $address->name, array('readonly')) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<p>Are you sure you wish to delete this Address?</p>

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Delete', array('class' => 'button medium radius')) }}
					<a href="{{ action('AddressController@index') }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
@stop
