@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-8 columns">
			<div class="index-buttons">
				<ul class="button-group radius right">
					<li><a href="{{ URL::action('AddressController@create') }}"" title="Add Address" class="button secondary small"><i class="icon-plus icon-large"></i>Add Address</a></li>
					<li><a href="{{ URL::action('DomainController@index') }}"" title="Manage Domains" class="button secondary small"><i class="icon-compass icon-large"></i>Manage Domains</a></li>
				</ul>
			</div>

			<h2>Addresses</h2>
			<table class="dns-records">
				<thead>
				<tr>
					<th>Alias</th>
					<th>IP Address</th>
					<th>Options</th>
				</tr>
				</thead>
				<tbody>
				@foreach($addresses as $address)
					<tr>
						<td class="breakable">{{ link_to_action('AddressController@edit', $address->name, array($address->id)) }}</td>
						<td class="text-center">{{ $address->address }}</td>
						<td class="options">
							<ul>
								<li><a href="{{ action('AddressController@edit', array('address' => $address->id)) }}" title="Edit Address"><i class="icon-pencil icon-large"></i></a></li>
								<li><a href="{{ action('AddressController@delete', array('id' => $address->id)) }}" title="Delete Address"><i class="icon-remove icon-large"></i></a></li>
							</ul>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop
