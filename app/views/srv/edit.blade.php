@extends('layouts.base')

{{-- Home page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $linode->getDomainName() }}</h1>

			@if ($srv->resourceid > 0)
				<h2>Edit SRV Record</h2>
				{{ Form::model($srv, array('route' => array('linode.srv.update', 'linode' => $srv->domainid, 'srv' => $srv->resourceid), 'method' => 'PUT', 'class' => 'custom')) }}
			@else
				<h2>Create SRV Record</h2>
				{{ Form::model($srv, array('route' => array('linode.srv.store', 'linode' => $srv->domainid), 'method' => 'POST', 'class' => 'custom')) }}
			@endif

			<!-- Service / Name -->
			<div class="row collapse {{{ $errors->has('name') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('name', 'Service') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('name', $srv->name) }}
					{{ $errors->first('name', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Protocol -->
			<div class="row collapse {{{ $errors->has('protocol') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('protocol', 'Protocol') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::select('protocol', $protocols, $srv->protocol) }}
					{{ $errors->first('protocol', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Mail Server / Target -->
			<div class="row collapse {{{ $errors->has('target') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('target', 'Target') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('target', $srv->target) }}
					{{ $errors->first('target', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;</div>
			</div>

			<!-- Priority -->
			<div class="row collapse {{{ $errors->has('priority') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('priority', 'Priority') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('priority', $srv->priority) }}
					{{ $errors->first('priority', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;0-255 (optional)</div>
			</div>

			<!-- Weight -->
			<div class="row collapse {{{ $errors->has('weight') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('weight', 'Weight') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('weight', $srv->weight) }}
					{{ $errors->first('weight', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;0-255 (optional)</div>
			</div>

			<!-- Port -->
			<div class="row collapse {{{ $errors->has('port') ? 'error' : '' }}}">
				<div class="large-2 columns">
					<span class="prefix radius">{{ Form::label('port', 'Port') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::text('port', $srv->port) }}
					{{ $errors->first('port', '<small>:message</small>') }}
				</div>
				<div class="large-7 columns">&nbsp;1-65535 (optional)</div>
			</div>

			@if ($srv->resourceid > 0)
				<!-- TTL -->
				<div class="row collapse {{{ $errors->has('ttl_sec') ? 'error' : '' }}}">
					<div class="large-2 columns">
						<span class="prefix radius">{{ Form::label('ttl_sec', 'TTL') }}</span>
					</div>

					<div class="large-3 columns">
						{{ Form::select('ttl_sec', $times, $srv->ttl_sec) }}
						{{ $errors->first('ttl_sec', '<small>:message</small>') }}
					</div>
					<div class="large-7 columns">&nbsp;</div>
				</div>
			@endif

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Save', array('class' => 'button medium radius')) }}
					<a href="{{ action('LinodeController@show', array('linode' => $srv->domainid)) }}" title="Cancel" class="button secondary medium radius">Cancel</a>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>
@stop
