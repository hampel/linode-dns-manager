<?php
/**
 * 
 */

return array(

	/*
	|--------------------------------------------------------------------------
	| Cache Expiry
	|--------------------------------------------------------------------------
	|
	| How long should the application cache responses from the API - in minutes
	| Default: 1440 minutes = 1 day
	|
	*/

	'cache_expiry' => 1440,

	/*
	|--------------------------------------------------------------------------
	| SOA Email
	|--------------------------------------------------------------------------
	|
	| Default Start of Authority (SOA) Email Address, ie - the email address of
	| the domain administrator
	|
	*/

	'soa_email' => '',

	/*
	|--------------------------------------------------------------------------
	| Default TTL
	|--------------------------------------------------------------------------
	|
	| Default TTL value for SOA records, in seconds
	| Any domain records set to "Default" will use this value for TTL
	|
	*/

	'default_ttl' => '86400',

	/*
	|--------------------------------------------------------------------------
	| Bulk TLDs
	|--------------------------------------------------------------------------
	|
	| Top Level Domains useable for bulk-add alias operations
	| array, include leading dot (eg .com, .com.au, .biz)
	|
	*/

	'bulk_tlds' => array(
		'.com',
		'.co',
		'.net',
	),


);

?>
