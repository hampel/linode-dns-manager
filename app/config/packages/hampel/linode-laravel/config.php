<?php
/**
 * Configuration for Linode
 */

return array(

	/*
	|--------------------------------------------------------------------------
	| Linode API Key
	|--------------------------------------------------------------------------
	|
	| Specify the API Key for your Linode account
	|
	*/

	'api_key' => '',

	/*
	|--------------------------------------------------------------------------
	| Cache Expiry
	|--------------------------------------------------------------------------
	|
	| How long should the application cache responses from the API - in minutes
	| Default: 1440 minutes = 1 day
	|
	*/

	'cache_expiry' => 1440

);

?>
