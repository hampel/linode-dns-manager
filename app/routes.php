<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Login / Logout forms
 */
Route::get('login', array('as' => 'login', 'uses' => 'AuthController@showLogin'));
Route::post('login', 'AuthController@postLogin');
Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

/**
 * Lost Password Form
 */
Route::get('lost-password', array('as' => 'lost-password', 'uses' => 'AuthController@showLostPassword'));
Route::post('lost-password', 'AuthController@postLostPassword');

/**
 * Password Reset Form
 */
Route::get('password-reset/{token}', array('as' => 'password-reset', 'uses' => 'AuthController@showPasswordReset'));
Route::post('password-reset/{token}', 'AuthController@postPasswordReset');

/**
 * Secure Routes
 */
Route::group(array('before' => 'auth'), function()
{
	Route::get('/', array('as' => 'home', 'uses' => 'DomainController@index'));

	/**
	 * Domains
	 */

	Route::get('linode/cache-refresh', array('as' => 'map-cache-refresh', function()
	{
		$domains = Linode::refreshLinodeDomainMap();
		if (is_null($domains)) App::abort(500, 'Could not update cache');

		return Redirect::route('home')->with('success', 'Domain map cache refreshed successfully');
	}));

	/**
	 * Linode controller
	 */
	Route::get('linode/{id}/cache-refresh', array('as' => 'domain-cache-refresh', function($id)
	{
		$map = Linode::refreshLinodeDomainMap();
		if (is_null($map)) App::abort(500, "Could not update cache for linode domain map");

		Resource::refreshResourceCache($id);

		return Redirect::action('LinodeController@show', array($id))->with('success', 'Domain cache refreshed successfully');
	}));

	Route::get('linode/{id}/delete', 'LinodeController@delete');
	Route::resource('linode', 'LinodeController', array('only' => array('index', 'show', 'destroy')));

	Route::get('domain/{id}/delete', 'DomainController@delete');
	Route::post('domain/{id}/aliases', 'DomainController@aliases');
	Route::resource('domain', 'DomainController');

	Route::get('address/{id}/delete', 'AddressController@delete');
	Route::resource('address', 'AddressController', array('only' => array('index', 'create', 'store', 'edit', 'update', 'destroy')));

	Route::resource('soa', 'Linode\SoaController', array('only' => array('edit', 'update')));

	Route::get('linode/{linode}/mx/{mx}/delete', 'Linode\MxController@delete');
	Route::resource('linode.mx', 'Linode\MxController', array('only' => array('create', 'store', 'edit', 'update', 'destroy')));

	Route::get('linode/{linode}/a/{a}/delete', 'Linode\AController@delete');
	Route::resource('linode.a', 'Linode\AController', array('only' => array('create', 'store', 'edit', 'update', 'delete', 'destroy')));

	Route::get('linode/{linode}/aaaa/{aaaa}/delete', 'Linode\AaaaController@delete');
	Route::resource('linode.aaaa', 'Linode\AaaaController', array('only' => array('create', 'store', 'edit', 'update', 'delete', 'destroy')));

	Route::get('linode/{linode}/cname/{cname}/delete', 'Linode\CnameController@delete');
	Route::resource('linode.cname', 'Linode\CnameController', array('only' => array('create', 'store', 'edit', 'update', 'destroy')));

	Route::get('linode/{linode}/txt/{txt}/delete', 'Linode\TxtController@delete');
	Route::resource('linode.txt', 'Linode\TxtController', array('only' => array('create', 'store', 'edit', 'update', 'destroy')));

	Route::get('linode/{linode}/ns/{ns}/delete', 'NsController@delete');
	Route::resource('linode.ns', 'NsController', array('only' => array('create', 'store', 'edit', 'update', 'destroy')));

	Route::get('linode/{linode}/srv/{srv}/delete', 'SrvController@delete');
	Route::resource('linode.srv', 'SrvController', array('only' => array('create', 'store', 'edit', 'update', 'destroy')));

	/**
	 * Change Password form
	 */
	Route::get('change-password', array('as' => 'change-password', 'uses' => 'AuthController@showChangePassword'));
	Route::post('change-password', 'AuthController@postChangePassword');

	/**
	 * Change Email form
	 */
	Route::get('change-email', array('as' => 'change-email', 'uses' => 'AuthController@showChangeEmail'));
	Route::post('change-email', 'AuthController@postChangeEmail');

});

?>
