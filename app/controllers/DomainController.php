<?php

class DomainController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		Domain::cleanInvalidDomains();

		$data['domains'] = Domain::orderBy('name')->get();
		$data['unmanaged'] = Linode::getUnmanagedDomains($data['domains']);
		$data['orphaned'] = Linode::getOrphanedDomains($data['domains']);
		$data['parents'] = Linode::getDomainParents($data['domains']);
		$data['aliases'] = Linode::getAliasCount($data['domains']);

		return View::make('domain.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['domain'] = Linode::createDomainFromLinode(Request::get('linodeid', 0), Request::get('parent', 0), Request::get('name', ""));
		$data['parents'] = Domain::getAllParents('');
		$data['orphaned'] = false;

		return View::make('domain.edit', $data);
	}

	/**
	 * Create all selected aliases
	 *
	 * @return Response
	 */
	public function aliases($id)
	{
		$domain = Domain::findOrFail($id);

		return Domain::createAliases($domain);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Domain::validateCreateDomain(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			return Linode::storeDomain(Input::all());
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('DomainController@create', array('linodeid' => Input::get('linodeid', 0)))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if (is_numeric($id)) $domain = Domain::findOrFail($id);
		else
		{
			$domain = Domain::where('name', '=', $id)->firstOrFail();
			$id = $domain->id;
		}

		$data['domain'] = $domain;
		if ($domain->parent == 0)
		{
			$data['children'] = Domain::where('parent', '=', $id)->orderBy('name')->get();
		}
		else
		{
			$data['parent'] = Domain::findOrFail($domain->parent);
			$data['siblings'] = Domain::where('parent', '=', $domain->parent)->where('id', '!=', $id)->orderBy('name')->get();
		}

		$linode = Linode::getLinodeDomain($domain->linodeid);
		$data['linode'] = (!empty($linode)) ? $linode->get() : null;

		$data['map'] = Address::getAddressMap();

		$data['mx'] = Linode\Mx::getAllRecords($domain->linodeid);
		$data['a'] = Linode\A::getAllRecords($domain->linodeid);
		$data['aaaa'] = Linode\Aaaa::getAllRecords($domain->linodeid);
		$data['cname'] = Linode\Cname::getAllRecords($domain->linodeid);
		$data['txt'] = Linode\Txt::getAllRecords($domain->linodeid);

		$data['base'] = $domain->getDomainBase();

		$data['aliases'] = Domain::getAvailableAliases($data['base']);
		$data['unmanaged'] = Linode::checkUnmanaged($data['base'], $data['aliases']);

		return View::make('domain.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['domain'] = Domain::findOrFail($id);
		$data['parents'] = Domain::getAllParents($data['domain']->name);
		$data['orphaned'] = !array_key_exists($data['domain']->linodeid, Linode::getLinodeDomainMap());
		if ($data['orphaned'])
		{
			$newlinodeid = Linode::findUpdatedDomain($data['domain']->name, $data['domain']->linodeid);
			if (!empty($newlinodeid))
			{
				$data['orphaned'] = false; // not really orphaned, just needs fixing
				$data['domain']->linodeid = $newlinodeid;
			}
		}

		return View::make('domain.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$domain = Domain::findOrFail($id);
		$validator = Domain::validateUpdateDomain(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			$domain->parent = Input::get('parent');
			if (Input::has('linodeid')) $domain->linodeid = Input::get('linodeid');
			$domain->save();

			if (!array_key_exists($domain->linodeid, Linode::getLinodeDomainMap()))
			{
				return Linode::recreateDomain($domain);
			}

			return Redirect::action('DomainController@show', array($domain->id))
				->with('success', "Domain {$domain->name} updated in database");
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('DomainController@edit', array('domain' => $id))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for confirming removal of specified resource
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		$data['domain'] = Domain::findOrFail($id);
		$children = Domain::where('parent', '=', $id)->orderBy('name')->get();

		if (!$children->isEmpty()) return Redirect::action('DomainController@show', array($id))
			->with('error', true)
			->with('reason', 'linode.delete.haschildren');

		return View::make('domain.delete', $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$domain = Domain::findOrFail($id);

		return Linode::deleteDomain($domain);
	}
}