<?php
/**
 * 
 */

class AuthController extends BaseController {

	/**
	 * Show Login Form
	 *
	 * @return mixed
	 */
	public function showLogin()
	{
		// Check if we already logged in
		if (Auth::check())
		{
			// Redirect to homepage
			return Redirect::intended('')->with('success', true)->with('reason', 'auth.login.already');
		}

		// Show the login page
		return View::make('auth.login');
	}

	/**
	 * Process Login form
	 *
	 * @return mixed
	 */
	public function postLogin()
	{
		// Get all the inputs
		// id is used for login, username is used for validation to return correct error-strings
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);

		// Declare the rules for the form validation.
		$rules = array(
			'username'  => 'required',
			'password'  => array('required', 'auth:' . Input::get('username'))
		);

		// Validate the inputs.
		$validator = Validator::make($userdata, $rules);

		// Check if the form validates with success.
		if ($validator->passes())
		{
			$userdata = array(
				'id'       => Input::get('username'),
				'password' => Input::get('password')
			);

			// Try to log the user in.
			if (Auth::attempt($userdata))
			{
				// Redirect to homepage
				return Redirect::intended('')->with('success', true)->with('reason', 'auth.login.successful');
			}
			else
			{
				// this shouldn't happen - we've already validated the credentials ... but just in case ...
				// Redirect to the login page.
				return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
			}
		}

		// Something went wrong.
		return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
	}

	/**
	 * Process log out request
	 *
	 * @return mixed
	 */
	public function getLogout()
	{
		// Log out
		Auth::logout();

		// Redirect to homepage
		return Redirect::route('home')->with('success', true)->with('reason', 'auth.login.loggedout');
	}

	/**
	 * Show Lost Password form
	 *
	 * @return mixed
	 */
	public function showLostPassword()
	{
		// Check if we already logged in
		if (Auth::check())
		{
			// Redirect to homepage
			return Redirect::route('home')->with('success', true)->with('reason', 'auth.reset.logoutfirst');
		}

		// Show the login page
		return View::make('auth.lostpassword');
	}

	/**
	 * Process Lost Password form
	 *
	 * @return mixed
	 */
	public function postLostPassword()
	{
		// Get all the inputs
		$userdata = array(
			'email' => Input::get('email'),
		);

		// Declare the rules for the form validation.
		$rules = array(
			'email'  => array('required', 'email', 'exists:users'),
		);

		// Validate the inputs.
		$validator = Validator::make($userdata, $rules);

		// Check if the form validates with success.
		if ($validator->passes())
		{
			$credentials = array('email' => Input::get('email'));

			return Password::remind($credentials);
		}

		// Something went wrong.
		return Redirect::route('lost-password')->withErrors($validator)->withInput(Input::only('email'));
	}

	/**
	 * Show Password Reset form
	 *
	 * @param $token
	 *
	 * @return mixed
	 */
	public function showPasswordReset($token)
	{
		// Check if we already logged in
		if (Auth::check())
		{
			// Redirect to homepage
			return Redirect::route('home')->with('success', true)->with('reason', 'auth.reset.logoutfirst');
		}

		// Show the password reset page
		return View::make('auth.reset', array('token' => $token));
	}

	/**
	 * Process Password Reset form
	 *
	 * @param $token
	 *
	 * @return mixed
	 */
	public function postPasswordReset($token)
	{
		// Get all the inputs
		$resetdata = array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			'password_confirmation' => Input::get('password_confirmation'),
		);

		// Declare the rules for the form validation.
		$rules = array(
			'email'  => array('required', 'email', 'exists:users', 'exists:password_reminders'),
			'password' => array('required', 'confirmed', 'between:8,64'),
		);

		// Validate the inputs.
		$validator = Validator::make($resetdata, $rules);

		// Check if the form validates with success.
		if ($validator->passes())
		{
			$credentials = array('email' => Input::get('email'));

			return Password::reset($credentials, function($user, $password)
			{
				$user->password = Hash::make($password);
				$user->save();

				$userdata = array(
					'id'       => $user->id,
					'password' => $password
				);

				// Try to log the user in.
				if (Auth::attempt($userdata))
				{
					// Redirect to homepage
					return Redirect::route('home')->with('success', true)->with('reason', 'auth.reset.success');
				}
				else
				{
					// this shouldn't happen ... but just in case ...
					// Redirect to the login page.
					return Redirect::route('login')->with('error', true)->with('reason', 'auth.reset.loginfail')->withInput(Input::except('password'));
				}
			});
		}

		return Redirect::route('password-reset', array('token' => Input::get('token')))->withErrors($validator)->withInput(Input::only('email', 'token'));
	}

	public function showChangePassword()
	{
		return View::make('auth.changepassword');
	}

	public function postChangePassword()
	{
		// Get all the inputs
		$passworddata = array(
			'password' => Input::get('password'),
			'new_password' => Input::get('new_password'),
			'new_password_confirmation' => Input::get('new_password_confirmation')
		);

		// Declare the rules for the form validation.
		$rules = array(
			'password'  => array('required', 'auth:' . Auth::user()->id),
			'new_password'  => array('required', 'confirmed', 'between:8,64')
		);

		// Validate the inputs.
		$validator = Validator::make($passworddata, $rules);

		// Check if the form validates with success.
		if ($validator->passes())
		{
			$password = Input::get('new_password');

			$user = Auth::user();
			$user->password = Hash::make($password);
			$user->save();

			$userdata = array(
				'id'       => $user->id,
				'password' => $password
			);

			// Try to log the user in.
			if (Auth::attempt($userdata))
			{
				// Redirect to homepage
				return Redirect::route('home')->with('success', true)->with('reason', 'auth.changepassword.success');
			}
			else
			{
				// this shouldn't happen ... but just in case ...
				// Redirect to the login page.
				return Redirect::route('login')->with('error', true)->with('reason', 'auth.changepassword.loginfail');
			}
		}

		return Redirect::route('change-password')->withErrors($validator);
	}

	public function showChangeEmail()
	{
		return View::make('auth.changeemail');
	}

	public function postChangeEmail()
	{
		// Get all the inputs
		$passworddata = array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
		);

		// Declare the rules for the form validation.
		$rules = array(
			'email' => array('required', 'email'),
			'password'  => array('required', 'auth:' . Auth::user()->id),
		);

		// Validate the inputs.
		$validator = Validator::make($passworddata, $rules);

		// Check if the form validates with success.
		if ($validator->passes())
		{
			$user = Auth::user();
			$user->email = Input::get('email');
			$user->save();

			return Redirect::route('home')->with('success', true)->with('reason', 'auth.changeemail.success');
		}

		return Redirect::route('change-email')->withErrors($validator);
	}
}

?>
