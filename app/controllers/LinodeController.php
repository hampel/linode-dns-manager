<?php

class LinodeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Redirect::action('DomainController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$domain = Domain::where('linodeid', '=', $id)->firstOrFail();

		Session::reflash();

		return Redirect::action('DomainController@show', array($domain->id));
	}

	/**
	 * Show the form for confirming removal of specified resource
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		$domain = Domain::where('linodeid', '=', $id)->first();
		if (!empty($domain)) return Redirect::action('DomainController@delete', array($domain->id));

		$data['linode'] = Linode::getLinodeDomain($id);
		if (empty($data['linode'])) App::abort(403, "Could not find Linode domain id {$id}");

		return View::make('linode.delete', $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$linode = Linode::getLinodeDomain($id);

		return Linode::deleteFromLinode($linode);
	}
}