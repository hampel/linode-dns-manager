<?php namespace Linode;

use App;
use View;
use Input;
use Redirect;
use Resource;
class TxtController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($linodeid)
	{
		$data['linode'] = \Linode::getLinodeDomain($linodeid);
		if (empty($data['linode'])) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$txt = new Txt;
		$txt->domainid = $linodeid;
		$data['txt'] = $txt;
		$data['times'] = Resource::getTimes();

		return View::make('txt.edit', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($linodeid)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$validator = Txt::validateTxt(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			return Txt::storeTxt($linodeid, Input::all());
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('Linode\TxtController@create', array('linode' => $linodeid))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($linodeid, $id)
	{
		$data['linode'] = \Linode::getLinodeDomain($linodeid);
		if (empty($data['linode'])) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Txt::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$data['txt'] = new Txt($resource->get());
		$data['times'] = Resource::getTimes();

		return View::make('txt.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$resource = Txt::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$validator = Txt::validateTxt(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			return Txt::updateTxt($linodeid, $id, Input::all());
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('Linode\TxtController@edit', array('linode' => $linodeid))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for confirming removal of specified resource
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Txt::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$data['txt'] = new Txt($resource->get());

		return View::make('txt.delete', $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Txt::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		return Txt::deleteRecord($linodeid, $id, $resource);
	}
}

?>
