<?php namespace Linode;

use App;
use View;
use Input;
use Redirect;
use Resource;
use Address;

class AaaaController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($linodeid)
	{
		$data['linode'] = \Linode::getLinodeDomain($linodeid);
		if (empty($data['linode'])) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$aaaa = new Aaaa;
		$aaaa->domainid = $linodeid;
		$data['aaaa'] = $aaaa;
		$data['times'] = Resource::getTimes();
		$data['addresses'] = Address::getAddressList("ipv6");

		return View::make('aaaa.edit', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($linodeid)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$validator = Aaaa::validateAaaa(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			return Aaaa::storeAaaa($linodeid, Input::all());
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('Linode\AAAAController@create', array('linode' => $linodeid))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($linodeid, $id)
	{
		$data['linode'] = \Linode::getLinodeDomain($linodeid);
		if (empty($data['linode'])) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Aaaa::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$data['aaaa'] = new Aaaa($resource->get());
		$data['times'] = Resource::getTimes();
		$data['addresses'] = Address::getAddressList("ipv6");

		return View::make('aaaa.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$resource = Aaaa::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$validator = Aaaa::validateAaaa(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			return Aaaa::updateAaaa($linodeid, $id, Input::all());
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('Linode\AAAAController@edit', array('linode' => $linodeid))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for confirming removal of specified resource
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Aaaa::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$data['aaaa'] = new Aaaa($resource->get());

		return View::make('aaaa.delete', $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Aaaa::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		return Aaaa::deleteRecord($linodeid, $id, $resource);
	}
}

?>
