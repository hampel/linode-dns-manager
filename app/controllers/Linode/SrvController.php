<?php namespace Linode;

use App;
use View;
use Input;
use Redirect;
use Resource;
class SrvController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($linodeid)
	{
		$data['linode'] = \Linode::getLinodeDomain($linodeid);
		if (empty($data['linode'])) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$srv = new Srv;
		$srv->domainid = $linodeid;
		$data['srv'] = $srv;
		$data['times'] = Resource::getTimes();
		$data['protocols'] = Resource::getProtocols();

		return View::make('srv.edit', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($linodeid)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$validator = Srv::validateSrv(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			return Srv::storeSrv($linodeid, Input::all());
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('Linode\SrvController@create', array('linode' => $linodeid))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($linodeid, $id)
	{
		$data['linode'] = \Linode::getLinodeDomain($linodeid);
		if (empty($data['linode'])) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Srv::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$data['srv'] = new Srv($resource->get());
		$data['times'] = Resource::getTimes();
		$data['protocols'] = Resource::getProtocols();

		return View::make('srv.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(403, "Could not find Linode domain id {$linodeid}");

		$resource = Srv::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$validator = Srv::validateSrv(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			return Srv::updateSrv($linodeid, $id, Input::all());
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('Linode\SrvController@edit', array('linode' => $linodeid))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for confirming removal of specified resource
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Srv::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		$data['srv'] = new Srv($resource->get());

		return View::make('srv.delete', $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($linodeid, $id)
	{
		$linode = \Linode::getLinodeDomain($linodeid);
		if (empty($linode)) App::abort(404, "Linode ID {$linodeid} not found");

		$resource = Srv::getRecord($linodeid, $id);
		if (empty($resource)) App::abort(404, "Resource ID {$id} not found");

		return Srv::deleteRecord($linodeid, $id, $resource);
	}
}

?>
