<?php namespace Linode;

use App;
use View;
use Input;
use Redirect;
use Resource;

class SoaController extends \BaseController {

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$linode = \Linode::getLinodeDomain($id);
		if (empty($linode)) App::abort(404, "Linode ID {$id} not found");

		$data['soa'] = new Soa($linode->get());
		$data['times'] = Resource::getTimes();
		$data['status'] = array(
			'1' => 'Active - Turn ON serving of this domain',
			'0' => 'Disabled - Turn OFF serving of this domain',
			'2' => 'Edit Mode - Use this mode while making edits'
		);

		return View::make('soa.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$linode = \Linode::getLinodeDomain($id);
		if (empty($linode)) App::abort(404, "Linode ID {$id} not found");

		$validator = Soa::validateSoa(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			return Soa::updateSoa($id, Input::all());
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('Linode\SoaController@edit', array('soa' => $id))
			->withErrors($validator)
			->withInput(Input::all());
	}

}