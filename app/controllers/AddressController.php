<?php

class AddressController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['addresses'] = Address::orderBy('name')->get();

		return View::make('address.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['address'] = new Address;
		$data['types'] = Address::getTypes();

		return View::make('address.edit', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Address::validateCreateAddress(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			$address = new Address(Input::all());
			$address->save();

			return Redirect::action('AddressController@index')
				->with('success', "Address {$address->name} added to database");
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('AddressController@create')
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['address'] = Address::findOrFail($id);
		$data['types'] = Address::getTypes();

		return View::make('address.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$address = Address::findOrFail($id);

		$validator = Address::validateCreateAddress(Input::all());

		// Check if the form validates with success.
		if ($validator->passes())
		{
			$address->fill(Input::all());
			$address->save();

			return Redirect::action('AddressController@index')
				->with('success', "Address {$address->name} updated successfully");
		}

		// validation failed ... redirect back to our create form
		return Redirect::action('AddressController@edit', array('address' => $id))
			->withErrors($validator)
			->withInput(Input::all());
	}

	/**
	 * Show the form for confirming removal of specified resource
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		$data['address'] = Address::findOrFail($id);

		return View::make('address.delete', $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$address = Address::findOrFail($id);
		$address->delete();

		return Redirect::action('AddressController@index')
			->with('success', "Address {$address->name} deleted successfully");
	}

}