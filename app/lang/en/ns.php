<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| NS Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'create' => array(
		'failure' => 'NS record creation failed: :message',
		'success' => 'NS record created successfully',
	),

	'update' => array(
		'failure' => 'NS record update failed: :message',
		'success' => 'NS record updated successfully',
	),
);