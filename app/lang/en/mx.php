<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| MX Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'create' => array(
		'failure' => 'MX record creation failed: :message',
		'success' => 'MX record created successfully',
	),

	'update' => array(
		'failure' => 'MX record update failed: :message',
		'success' => 'MX record updated successfully',
	),
);