<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| CNAME Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'create' => array(
		'failure' => 'CNAME record creation failed: :message',
		'success' => 'CNAME record created successfully',
	),

	'update' => array(
		'failure' => 'CNAME record update failed: :message',
		'success' => 'CNAME record updated successfully',
	),
);