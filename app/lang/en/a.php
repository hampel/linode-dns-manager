<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| A Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'create' => array(
		'failure' => 'A record creation failed: :message',
		'success' => 'A record created successfully',
	),

	'update' => array(
		'failure' => 'A record update failed: :message',
		'success' => 'A record updated successfully',
	),
);