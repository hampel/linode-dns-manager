<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Linode Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'create' => array(
		'failure' => 'Domain creation failed: :message',
		'success' => 'Domain created successfully',
	),

	'delete' => array(
		'failure' => 'Domain deletion failed: :message',
		'haschildren' => 'This Domain has children defined - remove them first before deleting the domain',
		'success' => 'Domain created successfully',
	),

	'alias' => array(
		'empty' => 'No alias TLDs specified',
		'invalid' => 'Invalid alias TLDs specified: :tld',
	),
);