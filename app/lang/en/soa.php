<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| SOA Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'update' => array(
		'failure' => 'SOA record update failed: :message',
		'success' => 'SOA record updated successfully',
	),

);