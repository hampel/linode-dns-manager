<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Auth Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'login' => array(
		'already' => 'You are already logged in',
		'successful' => 'You have logged in successfully',
		'loggedout' => 'You are logged out',

		'error'     => 'Error',
	),

	'reset' => array(
		'logoutfirst' => 'You are already logged in - please log out to reset your password',
		'requested' => "Password reset request successful - please check your email to proceed with reset",
		'success' => "Password change successful - you have been logged in",
		'loginfail' => "Password change successful, but login failed",
	),

	'changepassword' => array(
		'success' => "Password change successful",
		'loginfail' => "Password change successful, but login failed",
	),

	'changeemail' => array(
		'success' => "Email change successful",
	),

);