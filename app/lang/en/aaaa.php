<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| A Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'create' => array(
		'failure' => 'AAAA record creation failed: :message',
		'success' => 'AAAA record created successfully',
	),

	'update' => array(
		'failure' => 'AAAA record update failed: :message',
		'success' => 'AAAA record updated successfully',
	),
);