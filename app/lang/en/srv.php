<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| SRV Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'create' => array(
		'failure' => 'SRV record creation failed: :message',
		'success' => 'SRV record created successfully',
	),

	'update' => array(
		'failure' => 'SRV record update failed: :message',
		'success' => 'SRV record updated successfully',
	),
);