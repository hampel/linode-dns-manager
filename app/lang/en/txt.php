<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| TXT Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'create' => array(
		'failure' => 'TXT record creation failed: :message',
		'success' => 'TXT record created successfully',
	),

	'update' => array(
		'failure' => 'TXT record update failed: :message',
		'success' => 'TXT record updated successfully',
	),
);